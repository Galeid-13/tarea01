package ModuloInterfaz;

import ConexionBD.BaseDatos;
import ConexionBD.Usuario;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.apache.commons.codec.digest.DigestUtils;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FrmDatosDespacho extends javax.swing.JFrame {

        BaseDatos bd;
        int idDespacho;
        int nroNota;
        int nroSerie;
        
        String nombreEmp = "Nombre";
        String rucEmp = "RUC";
        String direccionEmp = "Direccion";
        String departamentoEmp = "Departamento";
        String provinciaEmp = "Provincia";
        String ciudadEmp = "Ciudad";
        
        Usuario user = new Usuario();

        public FrmDatosDespacho() {
                initComponents();

                Cerrar();
        }

        //Constructor en el caso de Reimprimir Notas
        public FrmDatosDespacho(String dni, String placa, String odometro, String descripcion, String pUnitario,
                String cantidad, String importe, String unidades, String numero, String serie, String accion, String idVenta) {
                initComponents();

                bd = new BaseDatos();

                this.nroNota = Integer.parseInt(numero);
                this.nroSerie = Integer.parseInt(serie);
                String nota = "0000000" + nroNota;
                String aux = "00" + nroSerie;
                txtSerieNota.setText("N" + aux.substring(aux.length() - 3, aux.length()) + "-" + nota.substring(nota.length() - 8, nota.length()));
                txtdniChofer.setText(dni);
                txtnombreChofer.setText(obtenerNombreChofer(dni));
                txtPlaca.setText(placa);
                txtOdometro.setText(String.format("%.2f", Double.parseDouble(odometro)));
                txtDescripcion.setText(descripcion);
                txtPrecioUnitario.setText(String.format("%.2f", Double.parseDouble(pUnitario)));
                txtCantidad.setText(String.format("%.2f", Double.parseDouble(cantidad)));
                txtImporte.setText(String.format("%.2f", Double.parseDouble(importe)));
                txtUnidad.setText(unidades);
                idDespacho = Integer.parseInt(idVenta);
                if (accion.equals("REIMPRIMIR")) {
                        btnImprimir.setText(accion);
                } else if (accion.equals("ANULAR")) {
                        idDespacho = Integer.parseInt(idVenta);
                        btnImprimir.setText(accion);
                }
                obtenerDatosGenerales();
                Cerrar();
        }

        //Constructor en el caso de Generar Notas
        public FrmDatosDespacho(int nroSerie, int nroNota, int idDesp, String descripcion, 
                String unid, String precioUni, String cantidad, String importe, String dniChofer, String placaVeh) {
                initComponents();
                
                bd = new BaseDatos();

                this.nroSerie = nroSerie;
                this.nroNota = nroNota;
                String nota = "0000000" + nroNota;
                String aux = "00" + nroSerie;
                txtSerieNota.setText("N" + aux.substring(aux.length() - 3, aux.length()) + "-" + nota.substring(nota.length() - 8, nota.length()));
                txtDescripcion.setText(descripcion);
                this.idDespacho = idDesp;
                txtUnidad.setText(unid);
                txtPrecioUnitario.setText(precioUni);
                txtCantidad.setText(cantidad);
                txtImporte.setText(importe);
                
                txtdniChofer.setText(dniChofer);
                txtPlaca.setText(placaVeh);
                txtnombreChofer.setText(obtenerNombreChofer(dniChofer));
                
                System.out.println(idDespacho + "id");
                btnImprimir.setText("IMPRIMIR");
                obtenerDatosGenerales();
                Cerrar();
        }
        
        public String obtenerNombreChofer(String dni){
            ResultSet rs = bd.Consultar("SELECT * FROM \"aChoferes\" WHERE \"chDNI\" = " + dni, null);
            try {
                rs.next();
                return(rs.getString("chNombre").trim());
            } catch (Exception e) {
                System.out.println("Error al obtener datos generales de la empresa");
                e.printStackTrace();
            }
            return "";
        }
        
        public void obtenerDatosGenerales(){
            ResultSet rs = bd.Consultar("SELECT * FROM \"aDatos\" WHERE \"dtID\" = 1", null);
            try {
                rs.next();
                nombreEmp = rs.getString("dtNombre").trim();
                rucEmp = rs.getString("dtRuc");
                direccionEmp = rs.getString("dtDireccion").trim();
                departamentoEmp = rs.getString("dtDepartamento").trim();
                provinciaEmp = rs.getString("dtProvincia").trim();
                ciudadEmp = rs.getString("dtCiudad").trim();
            } catch (Exception e) {
                System.out.println("Error al obtener datos generales de la empresa");
                e.printStackTrace();
            }
        }

        //Metodo para detectar cuando se cierre la ventana
        public void Cerrar() {
                try {
                        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                        addWindowListener(new WindowAdapter() {
                                public void windowClosing(WindowEvent e) {
                                        dispose();
                                }
                        });
                } catch (Exception e) {
                        System.out.println("Error" + e);
                }
        }

        public void impresionNota(){
            String dest = "C:/Notas/nota.pdf";
            try {
                int cara = Integer.parseInt(obtenerCara()) + 1;
                PdfWriter writer = new PdfWriter(dest);
                PdfDocument pdfDoc = new PdfDocument(writer);
                Document document = new Document(pdfDoc, PageSize.A7);
                PdfFont font = PdfFontFactory.createFont(FontConstants.COURIER);
                pdfDoc.addNewPage();
                document.setMargins(15, 20, 15, 20);
                String productoPara = txtDescripcion.getText().toUpperCase() + "                  ";
                productoPara = productoPara.substring(0, 14);
                String cantidadPara = txtCantidad.getText() + "         ";
                cantidadPara = cantidadPara.substring(0, 8);
                String importePara = txtImporte.getText() + "       ";
                importePara = importePara.substring(0, 7);
                Date sysFecha = new Date();
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/YYYY hh:mm:ss");
                
                Paragraph para = new Paragraph("ESTACIÓN DE SERVICIO\n"
                        + "'" + nombreEmp.toUpperCase() + "'\n"
                        + "RUC. " + rucEmp + "\n"
                        + direccionEmp.toUpperCase() + "\n"
                        + departamentoEmp.toUpperCase() + " " + provinciaEmp.toUpperCase() + " " + ciudadEmp.toUpperCase());
                Paragraph para2 = new Paragraph("NOTA DE DESPACHO\n"
                        + "N°" + txtSerieNota.getText());
                Paragraph para3 = new Paragraph(formato.format(sysFecha));
                Paragraph para4 = new Paragraph("NOMBRE: " + txtnombreChofer.getText().toUpperCase() + "\n"
                        + "PLACA: " + txtPlaca.getText().trim() + " ODOMETRO: " + txtOdometro.getText() + "\n"
                        + "CARA DESPACHO: " + cara + "\n"
                        + "GRIFERO: " + user.getUsNombre().toUpperCase() + "\n"
                        + "TURNO: " + user.getUsTurno() + "\n"
                        + "-----------------------------------\n"
                        + "PRODUCTO      CANTI.  UND   TOTAL  \n"
                        + "-----------------------------------\n"
                        + productoPara + cantidadPara + "GLS   " + importePara);
                para.setFontSize(9);
                para.setTextAlignment(TextAlignment.CENTER);
                para.setFont(font);
                para2.setFontSize(9);
                para2.setTextAlignment(TextAlignment.CENTER);
                para2.setFont(font);
                para3.setFontSize(8);
                para3.setTextAlignment(TextAlignment.CENTER);
                para3.setFont(font);
                para4.setFontSize(8);
                para4.setFont(font);
                
                document.add(para);
                document.add(para2);
                document.add(para3);
                document.add(para4);
                document.close();
                System.out.println("pdf Creado");
                
            } catch (Exception e) {
                System.out.println("Error al Crear PDF");
                e.printStackTrace();
            }
            try {
                if((new File("c:\\Notas/nota.pdf")).exists()){
                    Process p = Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler c:\\Notas/nota.pdf");
                    p.waitFor();
                } else {
                    System.out.println("El archivo no existe");
                }
                System.out.println("Hecho");
            } catch (Exception e) {
                System.out.println("Error al Abrir PDF");
                e.printStackTrace();
            }
        }
        
        public String obtenerCara(){
            ResultSet rs = bd.Consultar("SELECT \"veCara\" FROM \"aVenta\" WHERE \"veID\" = " + idDespacho, null);
            try {
                rs.next();
                String cara = rs.getString(1);
                return cara;
            } catch (Exception e) {
                System.out.println("Error al obtener cara");
                e.printStackTrace();
            }
            return "ERROR";
        }
        
        public String obtenerIDChofer(String dni){
            ResultSet rs = bd.Consultar("SELECT \"chID\" FROM \"aChoferes\" WHERE \"chDNI\" = " + dni, null);
            try {
                rs.next();
                if(rs.getString(1) != null){
                    return rs.getString(1);
                }
            } catch (Exception e) {
                System.out.println("Error al obtener idchofer");
                e.printStackTrace();
            }
            return "0";
        }
        
        public String obtenerIDVehiculo(String placa){
            ResultSet rs = bd.Consultar("SELECT \"vhID\" FROM \"aVehiculos\" WHERE \"vhPlaca\" = '" + placa + "'", null);
            try {
                rs.next();
                if(rs.getString(1) != null){
                    return rs.getString(1);
                }
            } catch (Exception e) {
                System.out.println("Error al obtener idvehiculo");
                e.printStackTrace();
            }
            return "0";
        }
        
        //edit
        public void actionButton() {
                if (btnImprimir.getText().equals("IMPRIMIR")) {
                        String idChofer = obtenerIDChofer(txtdniChofer.getText());
                        String placa = obtenerIDVehiculo(txtPlaca.getText());
                        Double odometro = Double.parseDouble(txtOdometro.getText());

                        String sql = "INSERT INTO \"aNotas\" (\"ntIDChofer\",\"ntIDVehiculo\",\"ntOdometro\","
                                + "\"ntNroSerie\",\"ntNroNota\",\"ntIDVenta\") "
                                + "VALUES (" + idChofer + ","
                                 + placa + ","
                                + odometro + ","
                                + nroSerie + ","
                                + nroNota + ","
                                + idDespacho + ")";
                        bd.Actualizar(sql, null);

                        String sql2 = "UPDATE \"aVenta\" SET \"veEstado\" = 1 WHERE \"veID\" = " + idDespacho;
                        bd.Actualizar(sql2, null);
                        //Aca viene la funcion de impresión
                        impresionNota();
                        JOptionPane.showMessageDialog(this, "Se generó el pdf correctamente");
                        dispose();
                } else if (btnImprimir.getText().equals("REIMPRIMIR")) {
                        //Aca viene la funcion de impresión
                        impresionNota();
                        JOptionPane.showMessageDialog(this, "Se regeneró el pdf correctamente");
                        dispose();
                } else if (btnImprimir.getText().equals("ANULAR")) {
                        Usuario user = new Usuario();
                        String motivo = JOptionPane.showInputDialog(this, "Ingresar motivo de anulacion", "Observacion", 3);
                        if (motivo.equals("")) {
                                JOptionPane.showMessageDialog(null, "Ingrese Motivo");
                        } else {
                                String password = JOptionPane.showInputDialog(null, "Ingresar Contraseña", user.getUsNickname(), 3);
                                if (password.equals("")) {
                                         JOptionPane.showMessageDialog(null, "Ingrese Contraseña");
                                }else{
                                        String dato = password;
                                        String cifrarPassword = DigestUtils.sha1Hex(dato);
                                        if (cifrarPassword.equals(user.getUsPassword())) {
                                                try {
                                                        String sql = "UPDATE \"aVenta\" set \"veEstado\"=0 WHERE \"veID\"=" + idDespacho;
                                                        bd.Actualizar(sql, null);
                                                        System.out.println(idDespacho + "ID de la venta editada");
                                                        String sql2 = "UPDATE \"aNotas\" set \"ntEstado\"='A', \"ntObservacion\"='"+motivo+"' WHERE \"ntNroSerie\"="+nroSerie+" AND \"ntNroNota\"="+nroNota;
                                                        bd.Actualizar(sql2, null);
                                                        //Aca viene la funcion de impresión
                                                        JOptionPane.showMessageDialog(this, "Se anulo el comprobante");
                                                        dispose();
                                                } catch (Exception e) {
                                                        System.out.println("Error al Anular");
                                                        e.printStackTrace();
                                                }
                                        } else {
                                                JOptionPane.showMessageDialog(null, "Contraseña Incorrecta", "ALERTA", 2);
                                        }
                                }
                        }
                }
        }

        public void Letras(KeyEvent evt) {
                char caracter = evt.getKeyChar();
                if (Character.isDigit(caracter)) {
                        getToolkit().beep();
                        evt.consume();
                        //JOptionPane.showMessageDialog(rootPane, "Ingresar solo letras");
                }
        }

        public void Numeros(KeyEvent evt) {
                char caracter = evt.getKeyChar();
                if (Character.isLetter(caracter)) {
                        getToolkit().beep();
                        evt.consume();
                        //JOptionPane.showMessageDialog(rootPane, "Ingresar solo Numeros");
                }
        }

        @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabel2 = new javax.swing.JLabel();
        txtSerieNota = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtdniChofer = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtOdometro = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtnombreChofer = new javax.swing.JTextField();
        txtPlaca = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtDescripcion = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtUnidad = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtPrecioUnitario = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtImporte = new javax.swing.JTextField();
        btnImprimir = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel2.setText("SERIE Y NÚMERO DE NOTA");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 110, 0, 0);
        getContentPane().add(jLabel2, gridBagConstraints);

        txtSerieNota.setEditable(false);
        txtSerieNota.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 7;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 200;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 33, 0, 0);
        getContentPane().add(txtSerieNota, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel3.setText("DNI CHOFER:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 0, 0);
        jPanel2.add(jLabel3, gridBagConstraints);

        txtdniChofer.setEditable(false);
        txtdniChofer.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        txtdniChofer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtdniChoferKeyTyped(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 100;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(15, 10, 0, 10);
        jPanel2.add(txtdniChofer, gridBagConstraints);

        jLabel4.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel4.setText("PLACA:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 0, 0);
        jPanel2.add(jLabel4, gridBagConstraints);

        jLabel5.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel5.setText("ODOMETRO:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 0, 0);
        jPanel2.add(jLabel5, gridBagConstraints);

        txtOdometro.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        txtOdometro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtOdometroKeyTyped(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 100;
        gridBagConstraints.insets = new java.awt.Insets(15, 10, 0, 10);
        jPanel2.add(txtOdometro, gridBagConstraints);

        jLabel6.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel6.setText("NOMBRE CHOFER:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 0, 0);
        jPanel2.add(jLabel6, gridBagConstraints);

        txtnombreChofer.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        txtnombreChofer.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtnombreChofer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnombreChoferKeyTyped(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.RELATIVE;
        gridBagConstraints.ipadx = 300;
        gridBagConstraints.insets = new java.awt.Insets(15, 10, 0, 10);
        jPanel2.add(txtnombreChofer, gridBagConstraints);

        txtPlaca.setEditable(false);
        txtPlaca.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 100;
        gridBagConstraints.insets = new java.awt.Insets(15, 10, 0, 15);
        jPanel2.add(txtPlaca, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 42;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 131, 0, 0);
        getContentPane().add(jPanel2, gridBagConstraints);

        jLabel7.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel7.setText("DESCRIPCION DEL PRODUCTO");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(20, 97, 0, 0);
        getContentPane().add(jLabel7, gridBagConstraints);

        txtDescripcion.setEditable(false);
        txtDescripcion.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.ipadx = 300;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 48, 0, 0);
        getContentPane().add(txtDescripcion, gridBagConstraints);

        jLabel8.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel8.setText("UNIDADES");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(20, 39, 0, 0);
        getContentPane().add(jLabel8, gridBagConstraints);

        txtUnidad.setEditable(false);
        txtUnidad.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 6;
        gridBagConstraints.ipadx = 100;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 0);
        getContentPane().add(txtUnidad, gridBagConstraints);

        jLabel9.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel9.setText("P. UNITARIO");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 11;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(20, 33, 0, 0);
        getContentPane().add(jLabel9, gridBagConstraints);

        txtPrecioUnitario.setEditable(false);
        txtPrecioUnitario.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 11;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 8;
        gridBagConstraints.ipadx = 100;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 0);
        getContentPane().add(txtPrecioUnitario, gridBagConstraints);

        jLabel10.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel10.setText("CANTIDAD");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 19;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 21;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(20, 38, 0, 0);
        getContentPane().add(jLabel10, gridBagConstraints);

        txtCantidad.setEditable(false);
        txtCantidad.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 19;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 22;
        gridBagConstraints.ipadx = 100;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 0);
        getContentPane().add(txtCantidad, gridBagConstraints);

        jLabel11.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel11.setText("IMPORTE");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 82;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(20, 0, 0, 0);
        getContentPane().add(jLabel11, gridBagConstraints);

        txtImporte.setEditable(false);
        txtImporte.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 41;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 43;
        gridBagConstraints.ipadx = 100;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 49);
        getContentPane().add(txtImporte, gridBagConstraints);

        btnImprimir.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnImprimir.setText("IMPRIMIR");
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(30, 2, 90, 0);
        getContentPane().add(btnImprimir, gridBagConstraints);

        btnCancelar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnCancelar.setText("CANCELAR");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 11;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(30, 19, 90, 0);
        getContentPane().add(btnCancelar, gridBagConstraints);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        jLabel1.setText("DATOS DEL DESPACHO");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 20;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(90, 242, 0, 0);
        getContentPane().add(jLabel1, gridBagConstraints);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
            // TODO add your handling code here:
            dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed
            actionButton();
    }//GEN-LAST:event_btnImprimirActionPerformed

        private void txtnombreChoferKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreChoferKeyTyped
                Letras(evt);
        }//GEN-LAST:event_txtnombreChoferKeyTyped

        private void txtdniChoferKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdniChoferKeyTyped
                Numeros(evt);
        }//GEN-LAST:event_txtdniChoferKeyTyped

        private void txtOdometroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOdometroKeyTyped
                Numeros(evt);
        }//GEN-LAST:event_txtOdometroKeyTyped

        /**
         * @param args the command line arguments
         */
        public static void main(String args[]) {
                /* Set the Nimbus look and feel */
                //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
                /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
                 */
                try {
                        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                                if ("Nimbus".equals(info.getName())) {
                                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                                        break;
                                }
                        }
                } catch (ClassNotFoundException ex) {
                        java.util.logging.Logger.getLogger(FrmDatosDespacho.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                        java.util.logging.Logger.getLogger(FrmDatosDespacho.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                        java.util.logging.Logger.getLogger(FrmDatosDespacho.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                        java.util.logging.Logger.getLogger(FrmDatosDespacho.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
                //</editor-fold>
                //</editor-fold>

                /* Create and display the form */
                java.awt.EventQueue.invokeLater(new Runnable() {
                        public void run() {
                                new FrmDatosDespacho().setVisible(true);
                        }
                });
        }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnImprimir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtDescripcion;
    private javax.swing.JTextField txtImporte;
    private javax.swing.JTextField txtOdometro;
    private javax.swing.JTextField txtPlaca;
    private javax.swing.JTextField txtPrecioUnitario;
    private javax.swing.JTextField txtSerieNota;
    private javax.swing.JTextField txtUnidad;
    private javax.swing.JTextField txtdniChofer;
    private javax.swing.JTextField txtnombreChofer;
    // End of variables declaration//GEN-END:variables
}
