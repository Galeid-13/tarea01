package ModuloInterfaz;

import ConexionBD.BaseDatos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

public class PnlCambioPrecios extends javax.swing.JPanel {

          BaseDatos bd = new BaseDatos();
          Connection conexion;
          int contadorInt = 0;
          ArrayList<Integer> idProductos;

          public PnlCambioPrecios() {
                    initComponents();
                    //Comprobar la conexión a la base de datos
                    conexion = bd.Conectar();
                    if (conexion == null) {
                              //---------------------------Cambiar a mensaje de error---------------------------
                              int valor = JOptionPane.showConfirmDialog(this, "No se pudo conectar con la base de datos, verifique "
                                      + "la conexión y reinicie el programa.", "Advertencia", JOptionPane.ERROR_MESSAGE);
                    }
                    idProductos = new ArrayList<>();

          }

          public void ActualizarDatos() {

                    ActualizarProductos();
          }

          public String loadConfig(String value) {
                    Properties config = new Properties();
                    InputStream configInput = null;
                    OutputStream configOutput = null;
                    try {
                              configInput = new FileInputStream("src/data/nameProducto.properties");
                              String valor;
                              //configInput = ClassLoader.getSystemResourceAsStream("data/nameProducto.properties");
                              config.load(configInput);
                              valor = config.getProperty(value);
                              configInput.close();
                              return valor;
                    } catch (Exception e) {
                              JOptionPane.showMessageDialog(null, "Error cargando configuración\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    return null;
          }

          public void escribirPropiedades(ArrayList<String> codigos, ArrayList<String> nombres) {
                    Properties p = new Properties();
                    InputStream propertiesStream = null;
                    OutputStream configOutput = null;
                    try {
                              configOutput = new FileOutputStream("src/data/nameProducto.properties");
                              //propertiesStream = ClassLoader.getSystemResourceAsStream("data/nameProducto.properties");
                              //propertiesStream = new FileInputStream("src/data/nameProducto.properties");
                              //p.load(propertiesStream);
                              //propertiesStream.close();
                              //configInput = ClassLoader.getSystemResourceAsStream("data/config.properties");
                              for (int i = 0; i < codigos.size(); i++) {
                                        p.setProperty(codigos.get(i), nombres.get(i));
                              }
                              
                              p.store(configOutput, "Cambios realizados");
                    } catch (Exception e) {
                              JOptionPane.showMessageDialog(null, "Error guardando configuración\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    }
          }

          public void ActualizarProductos() {
                    try {
                              idProductos.clear();
                              //DefaultTableModel modelo = new DefaultTableModel();
                              DefaultTableModel modelo;
                              //tblProductos.setModel(modelo);
                              String sql = "SELECT * FROM \"aProductos\" ORDER BY \"pdID\"";
                              ResultSet rs = bd.Consultar(sql, conexion);
                              Object[] nombresColumnas = new Object[]{"CODIGO", "NOMBRE PRODUCTO",
                                        "PRECIO CONTADO", "PRECIO CRÉDITO"};
                              //modelo.setColumnIdentifiers(nombresColumnas);
                              modelo = new DefaultTableModel(new Object[][]{}, nombresColumnas) {
                                        @Override
                                        public boolean isCellEditable(int fila, int columna) {
                                                  return (columna == 1);
                                        }
                              };
                              while (rs.next()) {
                                        idProductos.add(Integer.parseInt(rs.getString(1)));
                                        String producto = rs.getString("pdCodigo");
                                        String nombreProducto = rs.getString("pdCodigo");
                                        //Cargar nombre del producto de properties
                                        nombreProducto=loadConfig(producto);
                                        Object[] fila = new Object[]{
                                                  producto,
                                                  nombreProducto,
                                                  String.format("%.2f", rs.getDouble("pdPrecio1")).replace(',', '.'),
                                                  String.format("%.2f", rs.getDouble("pdPrecio2")).replace(',', '.'),};
                                        modelo.addRow(fila);
                                        tblProductos.setModel(modelo);
                                        tblProductos.getColumnModel().getColumn(0).setPreferredWidth(10);
                                        tblProductos.getColumnModel().getColumn(1).setPreferredWidth(175);

                              }
                              modelo.addTableModelListener(new TableModelListener() {
                                        @Override
                                        public void tableChanged(TableModelEvent e) {
                                                  if (e.getType() == TableModelEvent.UPDATE) {
                                                            ArrayList<String> listaCodigos = new ArrayList();
                                                            ArrayList<String> listaNombres = new ArrayList();
                                                            System.out.println("Hubo un cambio en la fila " + e.getFirstRow() + " y en la columna " + e.getColumn());
                                                            String codigo = (String) modelo.getValueAt(e.getFirstRow(), e.getColumn() - 1);
                                                            String valor = (String) modelo.getValueAt(e.getFirstRow(), e.getColumn());
                                                            for (int i = 0; i < tblProductos.getRowCount(); i++) {
                                                                      listaCodigos.add((String) tblProductos.getValueAt(i, 0));

                                                                      if(tblProductos.getValueAt(i, 1)==null){
                                                                                listaNombres.add("Ninguno");
                                                                      }else{
                                                                                listaNombres.add((String) tblProductos.getValueAt(i, 1));
                                                                      }     
                                                            }
                                                            
                                                            for (int i = 0; i < listaCodigos.size(); i++) {
                                                                      System.out.println("lista 1 "+listaCodigos.get(i));
                                                                      System.out.println("lista 2 "+listaNombres.get(i));
                                                            }
                                                            escribirPropiedades(listaCodigos, listaNombres);
                                                  } 
                                        }
                              });
                              return;
                    } catch (Exception ex) {
                              System.out.println("Error al actualizar los despachos");
                              ex.printStackTrace();
                    }
          }

          Timer contCon = new Timer(1000, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                              contadorInt += 1;
                              System.out.println("Se comenzó un evento del contador " + contadorInt);

                              try {
                                        ResultSet rs2 = bd.Consultar("SELECT \"cmValor\" FROM \"aComandos\" WHERE \"cmNumero\" = 2", conexion);
                                        rs2.next();
                                        if (Integer.parseInt(rs2.getString(1)) == 0) {
                                                  //Obtiene la ID, codigo, precio1, precio2 de la fila seleccionada
                                                  int filaSeleccionada = tblProductos.getSelectedRow();
                                                  int idProducto = idProductos.get(filaSeleccionada);
                                                  String producto = tblProductos.getValueAt(filaSeleccionada, 0).toString();
                                                  Double precioConta = Double.parseDouble(txtContado.getText().trim());
                                                  Double precioCredi = Double.parseDouble(txtCredito.getText().trim());

                                                  String sql = "UPDATE \"aProductos\" set \"pdPrecio1\"=" + precioConta + ",\"pdPrecio2\"=" + precioCredi + " WHERE \"pdID\"=" + idProducto + " AND \"pdCodigo\"='" + producto + "'";
                                                  //ResultSet rs3 = bd.Consultar(sql, conexion);
                                                  bd.Actualizar(sql, conexion);
                                                  contCon.stop();
                                                  contadorInt = 0;
                                                  ActualizarDatos();
                                                  JOptionPane.showMessageDialog(null, "Cambio Exitoso", "Confirmación", 2);

                                        }
                              } catch (Exception ex) {
                                        System.out.println("Error en el contador");
                                        ex.printStackTrace();
                              }

                              if (contadorInt == 5) {
                                        contCon.stop();
                                        contadorInt = 0;
                                        JOptionPane.showMessageDialog(null, "Esta desconectado el controlador, conéctelo e intente de nuevo", "ALERTA", 2);
                                        bd.Actualizar("UPDATE \"aComandos\" SET \"cmValor\" = 0 WHERE \"cmNumero\" = 2", conexion);
                                        //lblEspera.setVisible(false);
                              }

                    }
          });

          public void Numeros(KeyEvent evt) {
                    char caracter = evt.getKeyChar();
                    if (Character.isLetter(caracter)) {
                              getToolkit().beep();
                              evt.consume();
                              //JOptionPane.showMessageDialog(rootPane, "Ingresar solo Numeros");
                    }
          }

          @SuppressWarnings("unchecked")
          // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
          private void initComponents() {
                    java.awt.GridBagConstraints gridBagConstraints;

                    jLabel1 = new javax.swing.JLabel();
                    srcProductos = new javax.swing.JScrollPane();
                    tblProductos = new javax.swing.JTable();
                    jPanel1 = new javax.swing.JPanel();
                    btnAplicar = new javax.swing.JButton();
                    btnActualizar = new javax.swing.JButton();
                    txtCredito = new javax.swing.JTextField();
                    jLabel3 = new javax.swing.JLabel();
                    txtContado = new javax.swing.JTextField();
                    jLabel2 = new javax.swing.JLabel();
                    lblAdvertencia = new javax.swing.JLabel();
                    jLabel4 = new javax.swing.JLabel();

                    setLayout(new java.awt.GridBagLayout());

                    jLabel1.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
                    jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                    jLabel1.setText("CAMBIO DE PRECIOS");
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = 0;
                    gridBagConstraints.gridwidth = 12;
                    gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
                    gridBagConstraints.insets = new java.awt.Insets(0, 0, 30, 0);
                    add(jLabel1, gridBagConstraints);

                    tblProductos.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
                    tblProductos.setModel(new javax.swing.table.DefaultTableModel(
                              new Object [][] {
                                        {null, null, null, null},
                                        {null, null, null, null},
                                        {null, null, null, null},
                                        {null, null, null, null},
                                        {null, null, null, null},
                                        {null, null, null, null},
                                        {null, null, null, null}
                              },
                              new String [] {
                                        "CÓDIGO", "NOMBRE PRODUCTO", "PRECIO CONTADO", "PRECIO CRÉDITO"
                              }
                    ) {
                              Class[] types = new Class [] {
                                        java.lang.String.class, java.lang.String.class, java.lang.Double.class, java.lang.Double.class
                              };

                              public Class getColumnClass(int columnIndex) {
                                        return types [columnIndex];
                              }
                    });
                    tblProductos.addMouseListener(new java.awt.event.MouseAdapter() {
                              public void mouseClicked(java.awt.event.MouseEvent evt) {
                                        tblProductosMouseClicked(evt);
                              }
                    });
                    srcProductos.setViewportView(tblProductos);
                    if (tblProductos.getColumnModel().getColumnCount() > 0) {
                              tblProductos.getColumnModel().getColumn(0).setPreferredWidth(10);
                              tblProductos.getColumnModel().getColumn(1).setPreferredWidth(175);
                    }

                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = 3;
                    gridBagConstraints.gridwidth = 12;
                    gridBagConstraints.ipadx = 100;
                    gridBagConstraints.ipady = -250;
                    gridBagConstraints.insets = new java.awt.Insets(10, 0, 5, 0);
                    add(srcProductos, gridBagConstraints);

                    btnAplicar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                    btnAplicar.setText("APLICAR");
                    btnAplicar.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        btnAplicarActionPerformed(evt);
                              }
                    });

                    btnActualizar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                    btnActualizar.setText("ACTUALIZAR");
                    btnActualizar.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        btnActualizarActionPerformed(evt);
                              }
                    });

                    txtCredito.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                    txtCredito.addKeyListener(new java.awt.event.KeyAdapter() {
                              public void keyTyped(java.awt.event.KeyEvent evt) {
                                        txtCreditoKeyTyped(evt);
                              }
                    });

                    jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                    jLabel3.setText("CREDITO");

                    txtContado.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                    txtContado.addKeyListener(new java.awt.event.KeyAdapter() {
                              public void keyTyped(java.awt.event.KeyEvent evt) {
                                        txtContadoKeyTyped(evt);
                              }
                    });

                    jLabel2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                    jLabel2.setText("CONTADO");

                    lblAdvertencia.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
                    lblAdvertencia.setForeground(new java.awt.Color(255, 0, 0));
                    lblAdvertencia.setText("Es importante actualizar antes del Cambio de Precios");

                    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
                    jPanel1.setLayout(jPanel1Layout);
                    jPanel1Layout.setHorizontalGroup(
                              jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                              .addGap(0, 0, Short.MAX_VALUE)
                              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                  .addGap(0, 0, Short.MAX_VALUE)
                                                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                                      .addGap(30, 30, 30)
                                                                      .addComponent(lblAdvertencia))
                                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                                      .addComponent(jLabel2)
                                                                      .addGap(20, 20, 20)
                                                                      .addComponent(txtContado, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                      .addGap(20, 20, 20)
                                                                      .addComponent(jLabel3)
                                                                      .addGap(20, 20, 20)
                                                                      .addComponent(txtCredito, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                                      .addGap(46, 46, 46)
                                                                      .addComponent(btnAplicar)
                                                                      .addGap(96, 96, 96)
                                                                      .addComponent(btnActualizar)))
                                                  .addGap(0, 0, Short.MAX_VALUE)))
                    );
                    jPanel1Layout.setVerticalGroup(
                              jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                              .addGap(0, 0, Short.MAX_VALUE)
                              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                  .addGap(0, 0, Short.MAX_VALUE)
                                                  .addComponent(lblAdvertencia, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                  .addGap(17, 17, 17)
                                                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(txtContado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(txtCredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                                      .addGap(3, 3, 3)
                                                                      .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                .addComponent(jLabel2)
                                                                                .addComponent(jLabel3))))
                                                  .addGap(37, 37, 37)
                                                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(btnAplicar)
                                                            .addComponent(btnActualizar))
                                                  .addGap(0, 0, Short.MAX_VALUE)))
                    );

                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 9;
                    gridBagConstraints.gridy = 4;
                    gridBagConstraints.gridwidth = 3;
                    add(jPanel1, gridBagConstraints);

                    jLabel4.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
                    jLabel4.setText("Puedes editar el campo NOMBRE PRODUCTO para cambiar su valor en TODA la aplicación");
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 11;
                    gridBagConstraints.gridy = 2;
                    add(jLabel4, gridBagConstraints);
          }// </editor-fold>//GEN-END:initComponents

    private void btnAplicarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAplicarActionPerformed
              int filaSeleccionada = tblProductos.getSelectedRow();
              if (filaSeleccionada < 0) {
                        JOptionPane.showMessageDialog(this, "Seleccione un Producto");
              } else {
                        try {
                                  //String sql = "UPDATE \"aProductos\" set \"pdPrecio1\"="+precioConta+",\"pdPrecio2\"="+precioCredi+" WHERE \"pdID\"=" + idProducto+" AND \"pdCodigo\"="+producto ;
                                  String sql = "UPDATE \"aComandos\" SET \"cmValor\" = 1 WHERE \"cmNumero\" = 2";
                                  //ResultSet rs = bd.Consultar(sql, conexion);
                                  bd.Actualizar(sql, conexion);

                                  contCon.start();

                                  return;
                        } catch (Exception ex) {
                                  System.out.println("Error al actualizar los despachos");
                                  ex.printStackTrace();
                        }
              }


    }//GEN-LAST:event_btnAplicarActionPerformed

          private void tblProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblProductosMouseClicked
                    // int seleccion = tblProductos.rowAtPoint(evt.getPoint());
                    int seleccion = tblProductos.getSelectedRow();
                    txtContado.setText(String.valueOf(tblProductos.getValueAt(seleccion, 2)));
                    txtCredito.setText(String.valueOf(tblProductos.getValueAt(seleccion, 3)));
          }//GEN-LAST:event_tblProductosMouseClicked

          private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
                    ActualizarDatos();
          }//GEN-LAST:event_btnActualizarActionPerformed

          private void txtContadoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtContadoKeyTyped
                    Numeros(evt);
          }//GEN-LAST:event_txtContadoKeyTyped

          private void txtCreditoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCreditoKeyTyped
                    Numeros(evt);
          }//GEN-LAST:event_txtCreditoKeyTyped


          // Variables declaration - do not modify//GEN-BEGIN:variables
          private javax.swing.JButton btnActualizar;
          private javax.swing.JButton btnAplicar;
          private javax.swing.JLabel jLabel1;
          private javax.swing.JLabel jLabel2;
          private javax.swing.JLabel jLabel3;
          private javax.swing.JLabel jLabel4;
          private javax.swing.JPanel jPanel1;
          private javax.swing.JLabel lblAdvertencia;
          private javax.swing.JScrollPane srcProductos;
          private javax.swing.JTable tblProductos;
          private javax.swing.JTextField txtContado;
          private javax.swing.JTextField txtCredito;
          // End of variables declaration//GEN-END:variables
}
