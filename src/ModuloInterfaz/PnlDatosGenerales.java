/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloInterfaz;

import ConexionBD.BaseDatos;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Coaguila
 */
public class PnlDatosGenerales extends javax.swing.JPanel {

    BaseDatos bd = new BaseDatos();
    
    public PnlDatosGenerales() {
        initComponents();
        
        ResultSet rs = bd.Consultar("SELECT * FROM \"aDatos\" WHERE \"dtID\" = 1", null);
        try {
            rs.next();
            txtNombre.setText(rs.getString("dtNombre").trim());
            txtRuc.setText(rs.getString("dtRuc").trim());
            txtDireccion.setText(rs.getString("dtDireccion").trim());
            txtDepartamento.setText(rs.getString("dtDepartamento").trim());
            txtProvincia.setText(rs.getString("dtProvincia").trim());
            txtCiudad.setText(rs.getString("dtCiudad").trim());
            txtSerie.setText(rs.getString("dtSerie").trim());
        } catch (Exception e) {
            System.out.println("Error al obtener datos generales de la empresa");
            e.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtRuc = new javax.swing.JTextField();
        txtDireccion = new javax.swing.JTextField();
        txtDepartamento = new javax.swing.JTextField();
        btnActualizar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtProvincia = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtCiudad = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtSerie = new javax.swing.JTextField();

        setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel1.setText("Nombre Completo:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 15, 0);
        add(jLabel1, gridBagConstraints);

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel2.setText("RUC:");
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 15, 0);
        add(jLabel2, gridBagConstraints);

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel3.setText("Direccion:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 15, 0);
        add(jLabel3, gridBagConstraints);

        jLabel4.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel4.setText("Departamento:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 15, 0);
        add(jLabel4, gridBagConstraints);

        txtNombre.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtNombre.setMinimumSize(new java.awt.Dimension(250, 25));
        txtNombre.setPreferredSize(new java.awt.Dimension(250, 25));
        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 20);
        add(txtNombre, gridBagConstraints);

        txtRuc.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtRuc.setMinimumSize(new java.awt.Dimension(250, 25));
        txtRuc.setPreferredSize(new java.awt.Dimension(250, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 20);
        add(txtRuc, gridBagConstraints);

        txtDireccion.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtDireccion.setMinimumSize(new java.awt.Dimension(250, 25));
        txtDireccion.setPreferredSize(new java.awt.Dimension(250, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 20);
        add(txtDireccion, gridBagConstraints);

        txtDepartamento.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtDepartamento.setMinimumSize(new java.awt.Dimension(250, 25));
        txtDepartamento.setPreferredSize(new java.awt.Dimension(250, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 20);
        add(txtDepartamento, gridBagConstraints);

        btnActualizar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnActualizar.setText("Actualizar Datos");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 15;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipadx = 10;
        gridBagConstraints.ipady = 10;
        gridBagConstraints.insets = new java.awt.Insets(20, 0, 20, 0);
        add(btnActualizar, gridBagConstraints);

        jLabel5.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel5.setText("Datos Generales de la Empresa");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(23, 0, 23, 0);
        add(jLabel5, gridBagConstraints);

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel6.setText("Provincia:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 15, 0);
        add(jLabel6, gridBagConstraints);

        txtProvincia.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtProvincia.setMinimumSize(new java.awt.Dimension(250, 25));
        txtProvincia.setPreferredSize(new java.awt.Dimension(250, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 20);
        add(txtProvincia, gridBagConstraints);

        jLabel7.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel7.setText("Ciudad:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 15, 0);
        add(jLabel7, gridBagConstraints);

        txtCiudad.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtCiudad.setMinimumSize(new java.awt.Dimension(250, 25));
        txtCiudad.setPreferredSize(new java.awt.Dimension(250, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 20);
        add(txtCiudad, gridBagConstraints);

        jLabel8.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel8.setText("Número de Serie:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 15, 0);
        add(jLabel8, gridBagConstraints);

        txtSerie.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtSerie.setMinimumSize(new java.awt.Dimension(250, 25));
        txtSerie.setPreferredSize(new java.awt.Dimension(250, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 20);
        add(txtSerie, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        // TODO add your handling code here:
        String sql = "UPDATE \"aDatos\" SET \"dtNombre\" = '" + txtNombre.getText() + "', "
                + "\"dtRuc\" = '" + txtRuc.getText() + "', "
                + "\"dtDireccion\" = '" + txtDireccion.getText() + "', "
                + "\"dtDepartamento\" = '" + txtDepartamento.getText() + "', "
                + "\"dtProvincia\" = '" + txtProvincia.getText() + "', "
                + "\"dtCiudad\" = '" + txtCiudad.getText() + "', "
                + "\"dtSerie\" = " + txtSerie.getText() + " WHERE \"dtID\" = 1";
        bd.Actualizar(sql, null);
        JOptionPane.showMessageDialog(this, "Se actualizó la información correctamente");
    }//GEN-LAST:event_btnActualizarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JTextField txtCiudad;
    private javax.swing.JTextField txtDepartamento;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtProvincia;
    private javax.swing.JTextField txtRuc;
    private javax.swing.JTextField txtSerie;
    // End of variables declaration//GEN-END:variables
}
