/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloInterfaz;

import ConexionBD.BaseDatos;
import groovy.xml.Entity;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Coaguila
 */
public class FrmDatosFormulario extends javax.swing.JFrame {

    String nombreTabla = "";
    BaseDatos bd = new BaseDatos();
    Connection conexion;
    int idObject = 0;
    
    public FrmDatosFormulario() {
        initComponents();
        Cerrar();
        this.setTitle("Insertar Datos");
        this.setIconImage(new ImageIcon(getClass().getResource("/image/Proyecto.png")).getImage());
    }
    
    public FrmDatosFormulario(String label1, String label2, String label3, String label4, String label5, String accion, String nomTabla, String titulo){
        initComponents();
        this.nombreTabla = nomTabla;
        conexion = bd.Conectar();
        lblTitulo.setText(titulo);
        if(label1.equals("0")){
            lbl1.setVisible(false);
            txt1.setVisible(false);
        }else{
            lbl1.setText(label1);
        }
        if(label2.equals("0")){
            lbl2.setVisible(false);
            txt2.setVisible(false);
        }else{
            lbl2.setText(label2);
        }
        if(label3.equals("0")){
            lbl3.setVisible(false);
            txt3.setVisible(false);
        }else{
            lbl3.setText(label3);
        }
        if(label4.equals("0")){
            lbl4.setVisible(false);
            txt4.setVisible(false);
        }else{
            lbl4.setText(label4);
        }
        if(label5.equals("0")){
            lbl5.setVisible(false);
            txt5.setVisible(false);
        }else{
            lbl5.setText(label5);
        }
        btnAccion.setText(accion);
        if(nomTabla.equals("Choferes")){
            if(accion.equals("Editar")){
                txt3.addKeyListener(new KeyListener(){
                    public void keyTyped(KeyEvent e){
                        if (txt3.getText().length()== 1){
                            e.consume();
                        }
                    }

                    @Override
                    public void keyPressed(KeyEvent e) {
                         //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public void keyReleased(KeyEvent e) {
                        //To change body of generated methods, choose Tools | Templates.
                    }
                });
            }
        }
        if(nomTabla.equals("Vehiculos")){
            if(accion.equals("Editar")){
                txt4.addKeyListener(new KeyListener(){
                    public void keyTyped(KeyEvent e){
                        if (txt4.getText().length()== 1){
                            e.consume();
                        }
                    }

                    @Override
                    public void keyPressed(KeyEvent e) {
                         //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public void keyReleased(KeyEvent e) {
                        //To change body of generated methods, choose Tools | Templates.
                    }
                });
            }
        }
        Cerrar();
    }
    
    public void Cerrar() {
        try {
                this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                addWindowListener(new WindowAdapter() {
                        public void windowClosing(WindowEvent e) {
                                dispose();
                        }
                });
        } catch (Exception e) {
                System.out.println("Error" + e);
        }
    }
    
    public static boolean isNumeric(String str){
        if(str == null){
            return false;
        }
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
    
    public void AsignarDatos(String atxt1, String atxt2, String atxt3, String atxt4, String atxt5){
        if(atxt1 != null){
            txt1.setText(atxt1);
        }
        if(atxt2 != null){
            txt2.setText(atxt2);
        }
        if(atxt3 != null){
            txt3.setText(atxt3);
        }
        if(atxt4 != null){
            txt4.setText(atxt4);
        }
        if(atxt5 != null){
            txt5.setText(atxt5);
        }
    }
    
    public void setID(int id){
        this.idObject = id;
    }
    
    public void AgregarChofer(){
        String chNombre = txt1.getText();
        String chDNI = txt2.getText();
        
        if(isNumeric(chDNI)){
            bd.Actualizar("INSERT INTO \"aChoferes\" (\"chNombre\",\"chDNI\",\"chEstado\") VALUES ('" + chNombre + 
                "'," + chDNI + ", 'P')", conexion);
            JOptionPane.showMessageDialog(this, "Se agregó el chofer correctamente");
            dispose();
            System.out.println("Adición de Chofer correcta");
        }else{
            JOptionPane.showMessageDialog(this, "El DNI debe tener solo números");
        }
    }
    
    public void EditarChofer(){
        String chNombre = txt1.getText();
        String chDNI = txt2.getText();
        String chEstado = txt3.getText();
        
        if(isNumeric(chDNI)){
            if(chEstado.equals("P") || chEstado.equals("N")){
                bd.Actualizar("UPDATE \"aChoferes\" SET \"chNombre\" = '" + chNombre +
                    "', \"chDNI\" = " + chDNI + ", \"chEstado\" = '" + chEstado +
                    "' WHERE \"chID\" = " + idObject, conexion);
                JOptionPane.showMessageDialog(this, "Se editó el chofer correctamente");
                dispose();
            }
        }else{
            JOptionPane.showMessageDialog(this, "El DNI debe tener solo números");
        }
    }
    
    public void AgregarVehiculo(){
        String vhPlaca = txt1.getText();
        String vhMarca = txt2.getText();
        String vhDetalle = txt3.getText();
        
        bd.Actualizar("INSERT INTO \"aVehiculos\" (\"vhPlaca\",\"vhMarca\",\"vhDetalle\",\"vhEstado\") VALUES ('" + vhPlaca + 
                "','" + vhMarca + "','" + vhDetalle + "','P')", conexion);
        JOptionPane.showMessageDialog(this, "Se agregó el vehiculo correctamente");
        dispose();
        System.out.println("Adición de Vehiculo correcta");
    }
    
    public void EditarVehiculo(){
        String vhPlaca = txt1.getText();
        String vhMarca = txt2.getText();
        String vhDetalle = txt3.getText();
        String vhEstado = txt4.getText();
        if(vhEstado.equals("P") || vhEstado.equals("N")){
            bd.Actualizar("UPDATE \"aVehiculos\" SET \"vhPlaca\" = '" + vhPlaca +
                "', \"vhMarca\" = '" + vhMarca + "', \"vhDetalle\" = '" + vhDetalle + "', \"vhEstado\" = '" + vhEstado +
                "' WHERE \"vhID\" = " + idObject, conexion);
            JOptionPane.showMessageDialog(this, "Se editó el vehiculo correctamente");
            dispose();
        }
    }
    
    public void AgregarUsuario(){
        String usNombre = txt1.getText();
        String usDNI = txt2.getText();
        String usTipo = txt3.getText();
        String usNickname = txt4.getText();
        String usClave = txt5.getText();
        
        if(isNumeric(usDNI)){
            if(usTipo.equals("Administrador")||usTipo.equals("Usuario")){
                usClave = DigestUtils.sha1Hex(usClave);
                bd.Actualizar("INSERT INTO \"aUsuario\" (\"usNombre\",\"usDNI\",\"usTipo\",\"usNickname\",\"usPassword\") VALUES ('" + usNombre +
                    "'," + usDNI + ",'" + usTipo + "','" + usNickname + "','" + usClave + "')", conexion);
                JOptionPane.showMessageDialog(this, "Se agregó el usuario correctamente");
                dispose();
            }else{
                JOptionPane.showMessageDialog(this, "El tipo debe ser Administrador o Usuario");
            }
        }else{
            JOptionPane.showMessageDialog(this, "El DNI debe tener solo números");
        }
    }
    
    public void EditarUsuario(){
        String usNombre = txt1.getText();
        String usDNI = txt2.getText();
        String usTipo = txt3.getText();
        String usNick = txt4.getText();
        
        if(isNumeric(usDNI)){
            if(usTipo.equals("Administrador") || usTipo.equals("Usuario")){
                bd.Actualizar("UPDATE \"aUsuario\" SET \"usNombre\" = '" + usNombre +
                    "', \"usDNI\" = " + usDNI + ", \"usTipo\" = '" + usTipo +
                    "', \"usNickname\" = '" + usNick + "' WHERE \"usID\" = " + idObject, conexion);
                JOptionPane.showMessageDialog(this, "Se editó el usuario correctamente");
                dispose();
            }else{
                JOptionPane.showMessageDialog(this, "El tipo debe ser Administrador o Usuario");
            }
        }else{
            JOptionPane.showMessageDialog(this, "El DNI debe tener solo números");
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        lbl1 = new javax.swing.JLabel();
        txt1 = new javax.swing.JTextField();
        lbl2 = new javax.swing.JLabel();
        txt4 = new javax.swing.JTextField();
        txt2 = new javax.swing.JTextField();
        lbl3 = new javax.swing.JLabel();
        txt3 = new javax.swing.JTextField();
        lbl4 = new javax.swing.JLabel();
        lblTitulo = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnAccion = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        lbl5 = new javax.swing.JLabel();
        txt5 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(480, 420));
        getContentPane().setLayout(new java.awt.GridBagLayout());

        lbl1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lbl1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl1.setText("Label1");
        lbl1.setPreferredSize(new java.awt.Dimension(150, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 15, 0);
        getContentPane().add(lbl1, gridBagConstraints);

        txt1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txt1.setPreferredSize(new java.awt.Dimension(220, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 30);
        getContentPane().add(txt1, gridBagConstraints);

        lbl2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lbl2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl2.setText("Label2");
        lbl2.setPreferredSize(new java.awt.Dimension(150, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 15, 0);
        getContentPane().add(lbl2, gridBagConstraints);

        txt4.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txt4.setPreferredSize(new java.awt.Dimension(220, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 30);
        getContentPane().add(txt4, gridBagConstraints);

        txt2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txt2.setPreferredSize(new java.awt.Dimension(220, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 30);
        getContentPane().add(txt2, gridBagConstraints);

        lbl3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lbl3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl3.setText("Label3");
        lbl3.setPreferredSize(new java.awt.Dimension(150, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 15, 0);
        getContentPane().add(lbl3, gridBagConstraints);

        txt3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txt3.setPreferredSize(new java.awt.Dimension(220, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 30);
        getContentPane().add(txt3, gridBagConstraints);

        lbl4.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lbl4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl4.setText("Label4");
        lbl4.setPreferredSize(new java.awt.Dimension(150, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 15, 0);
        getContentPane().add(lbl4, gridBagConstraints);

        lblTitulo.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lblTitulo.setText("Titulo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 20, 0);
        getContentPane().add(lblTitulo, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        btnAccion.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnAccion.setText("Boton");
        btnAccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAccionActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 0, 0);
        jPanel1.add(btnAccion, gridBagConstraints);

        jButton2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButton2.setText("Salir");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.insets = new java.awt.Insets(15, 40, 0, 0);
        jPanel1.add(jButton2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 3;
        getContentPane().add(jPanel1, gridBagConstraints);

        lbl5.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lbl5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl5.setText("Label5");
        lbl5.setPreferredSize(new java.awt.Dimension(150, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 15, 0);
        getContentPane().add(lbl5, gridBagConstraints);

        txt5.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txt5.setPreferredSize(new java.awt.Dimension(220, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 30);
        getContentPane().add(txt5, gridBagConstraints);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnAccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAccionActionPerformed
        // TODO add your handling code here:
        if(nombreTabla.equals("Choferes")){
            if(btnAccion.getText().equals("Agregar")){
                AgregarChofer();
            }else if(btnAccion.getText().equals("Editar")){
                EditarChofer();
            }
        }else if(nombreTabla.equals("Vehiculos")){
            if(btnAccion.getText().equals("Agregar")){
                AgregarVehiculo();
            }else if(btnAccion.getText().equals("Editar")){
                EditarVehiculo();
            }
        }else if(nombreTabla.equals("Usuarios")){
            if(btnAccion.getText().equals("Agregar")){
                AgregarUsuario();
            }else if(btnAccion.getText().equals("Editar")){
                EditarUsuario();
            }
        }
    }//GEN-LAST:event_btnAccionActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmDatosFormulario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmDatosFormulario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmDatosFormulario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmDatosFormulario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmDatosFormulario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAccion;
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lbl1;
    private javax.swing.JLabel lbl2;
    private javax.swing.JLabel lbl3;
    private javax.swing.JLabel lbl4;
    private javax.swing.JLabel lbl5;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JTextField txt1;
    private javax.swing.JTextField txt2;
    private javax.swing.JTextField txt3;
    private javax.swing.JTextField txt4;
    private javax.swing.JTextField txt5;
    // End of variables declaration//GEN-END:variables
}
