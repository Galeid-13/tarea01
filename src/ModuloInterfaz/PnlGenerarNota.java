package ModuloInterfaz;

import ConexionBD.BaseDatos;
import ConexionBD.Usuario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

public class PnlGenerarNota extends javax.swing.JPanel {

        BaseDatos bd = new BaseDatos();
        Connection conexion;
        int nroNota = 1;
        int nroSerie = 1;
        ArrayList<Integer> idDespachos;
        String dniChofer = "";
        String placaVehiculo = "";

        public PnlGenerarNota() {
                initComponents();

                //Extraer el NOMBRE del usuario logueado
                Usuario user = new Usuario();
                txtGrifero.setText(user.getUsNombre());

                //Comprobar la conexión a la base de datos
                conexion = bd.Conectar();
                if (conexion == null) {
                        //---------------------------Cambiar a mensaje de error---------------------------
                        int valor = JOptionPane.showConfirmDialog(this, "No se pudo conectar con la base de datos, verifique "
                                + "la conexión y reinicie el programa.", "Advertencia", JOptionPane.ERROR_MESSAGE);
                }

                idDespachos = new ArrayList<>();

                Timer tiempo = new Timer(100, new PnlGenerarNota.Hora());
                tiempo.start();
        }
        
        public String loadConfig(String value) {
                    Properties config = new Properties();
                    InputStream configInput = null;
                    OutputStream configOutput = null;
                    try {
                              configInput = new FileInputStream("src/data/nameProducto.properties");
                              String valor;
                              //configInput = ClassLoader.getSystemResourceAsStream("data/nameProducto.properties");
                              config.load(configInput);
                              valor = config.getProperty(value);
                              configInput.close();
                              return valor;
                    } catch (Exception e) {
                              JOptionPane.showMessageDialog(null, "Error cargando configuración\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    return null;
          }

        //Metodo que actualiza los datos de los campos del Panel cada vez que sea visible
        public void ActualizarDatos() {
                Date sysFecha = new Date();
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/YYYY");
                txtFecha.setText(formato.format(sysFecha));
                lblAdvertencia.setVisible(false);
                ActualizarDespachos();
                ActualizarNroSerieNota();
                ActualizarTurno();
        }

        //Metodo para actualizar el turno
        public void ActualizarTurno(){
            String sql = "SELECT \"tnNumero\" FROM \"aTurnos\" WHERE \"tnID\" = (SELECT MAX(\"tnID\") from \"aTurnos\")";
            ResultSet rs = bd.Consultar(sql, conexion);
            try {
                rs.next();
                txtTurno.setText(rs.getString(1));
            } catch (Exception ex) {
                System.out.println("Error al obtener el turno");
                ex.printStackTrace();
            }
        }
        
        public int obtenerNroSerie(){
            ResultSet rs = bd.Consultar("SELECT \"dtSerie\" FROM \"aDatos\" WHERE \"dtID\" = 1", conexion);
            try {
                rs.next();
                int i = Integer.parseInt(rs.getString(1));
                return i;
            } catch (Exception ex) {
                System.out.println("Error al obtener el nroSerie");
                ex.printStackTrace();
            }
            return 1;
        }
        
        //Metodo para la actualizacion de NroSerie y NroNota
        public void ActualizarNroSerieNota() {
            System.out.println("c");
            nroSerie = obtenerNroSerie();
            if (bd.TablaSinDatos("aNotas")) {
                    txtSerie.setText("N00" + nroSerie);
                    txtNumero.setText("0000000" + nroNota);
                    System.out.println("b");
            } else {
                    try {
                            String sql = "SELECT * FROM \"aNotas\" WHERE \"ntID\" = (SELECT MAX(\"ntID\") from \"aNotas\")";
                            ResultSet rs = bd.Consultar(sql, conexion);
                            while (rs.next()) {
//                                    nroSerie = Integer.parseInt(rs.getString(6));
//                                    System.out.println(nroSerie);
                                    nroNota = Integer.parseInt(rs.getString(6)) + 1;
                                    String nota = "0000000" + nroNota;
                                    String aux = "00" + nroSerie;

                                    txtNumero.setText(nota.substring(nota.length() - 8, nota.length()));
                                    txtSerie.setText("N" + aux.substring(aux.length() - 3, aux.length()));
                                    System.out.println("a");
                            }
                            return;
                    } catch (Exception ex) {
                            System.out.println("Error al obtener Nro Serie y Nota");
                            ex.printStackTrace();
                    }
            }
        }

        //Metodo para actualizar la tabla de despachos no registrados
        public void ActualizarDespachos() {
                try {
                    System.out.println("a4");
                        idDespachos.clear();
                        DefaultTableModel modelo ;
                        //tblDespachos.setModel(modelo);
                        String sql = "SELECT * FROM \"aVenta\" WHERE \"veEstado\" = 0";
                        ResultSet rs = bd.Consultar(sql, conexion);
                        Object[] nombresColumnas = new Object[]{"FECHA", "HORA",
                                "CARA", "DESCRIPCION", "UND", "P.UNIT", "CANTIDAD", "IMPORTE"};
                        //modelo.setColumnIdentifiers(nombresColumnas);
                        System.out.println("a5");
                        modelo = new DefaultTableModel(new Object[][]{}, nombresColumnas) {
                                        @Override
                                        public boolean isCellEditable(int fila, int columna) {
                                                  return false;
                                        }
                              };
                        int a = 0;
                        System.out.println("a6");
                        while (rs.next()) {
                            a = 1;
                                idDespachos.add(Integer.parseInt(rs.getString(1)));
                                String producto = loadConfig(rs.getString("veProducto"));
                                Object[] fila = new Object[]{
                                        rs.getString("veHoraPC").substring(0, 10),
                                        rs.getString("veHoraPC").substring(11, 19),
                                        rs.getString("veCara"),
                                        producto,
                                        "GLS",
                                        String.format("%.2f", rs.getDouble("vePrecio")),
                                        String.format("%.2f", rs.getDouble("veVolumen")),
                                        String.format("%.2f", rs.getDouble("veImporte")),};
                                modelo.addRow(fila);
                                tblDespachos.setModel(modelo);
                        }
                        if(a == 0){
                            tblDespachos.setModel(modelo);
                        }else{
                            return;
                        }
                } catch (Exception ex) {
                        System.out.println("Error al actualizar los despachos");
                        ex.printStackTrace();
                }
        }

        class Hora implements ActionListener {

                public void actionPerformed(ActionEvent e) {
                        Date sysHora = new Date();
                        String pmAm = "hh:mm:ss a";
                        SimpleDateFormat format = new SimpleDateFormat(pmAm);
                        Calendar hoy = Calendar.getInstance();
                        txtHora.setText(String.format(format.format(sysHora), hoy));
                }
        }

        @SuppressWarnings("unchecked")

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txtSerie = new javax.swing.JTextField();
        txtFecha = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtHora = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtNumero = new javax.swing.JTextField();
        txtTurno = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtGrifero = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        scrDespachos = new javax.swing.JScrollPane();
        tblDespachos = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        btnAceptar = new javax.swing.JButton();
        btnActualizar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        lblAdvertencia = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        txtPlaca = new javax.swing.JTextField();
        txtDniChofer = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        btnVerificar = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(1280, 720));
        setLayout(new java.awt.GridBagLayout());

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel3.setText("DETALLES DE DESPACHO POR SURTIDORES");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 15, 0);
        add(jLabel3, gridBagConstraints);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("GENERAR NOTAS DE DESPACHO");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 20, 0);
        add(jLabel1, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        txtSerie.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        jPanel1.add(txtSerie, gridBagConstraints);

        txtFecha.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 60;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        jPanel1.add(txtFecha, gridBagConstraints);

        jLabel5.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel5.setText("NRO:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 1;
        jPanel1.add(jLabel5, gridBagConstraints);

        jLabel4.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel4.setText("FECHA:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel1.add(jLabel4, gridBagConstraints);

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel6.setText("HORA:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        jPanel1.add(jLabel6, gridBagConstraints);

        txtHora.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 60;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        jPanel1.add(txtHora, gridBagConstraints);

        jLabel7.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel7.setText("TURNO:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        jPanel1.add(jLabel7, gridBagConstraints);

        txtNumero.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        jPanel1.add(txtNumero, gridBagConstraints);

        txtTurno.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        jPanel1.add(txtTurno, gridBagConstraints);

        jLabel8.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel8.setText("SERIE");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        jPanel1.add(jLabel8, gridBagConstraints);

        jLabel9.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel9.setText("NÚMERO");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel1.add(jLabel9, gridBagConstraints);

        txtGrifero.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.RELATIVE;
        gridBagConstraints.ipadx = 450;
        gridBagConstraints.insets = new java.awt.Insets(20, 10, 0, 10);
        jPanel1.add(txtGrifero, gridBagConstraints);

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel2.setText("VENDEDOR:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.insets = new java.awt.Insets(20, 0, 0, 0);
        jPanel1.add(jLabel2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 8;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 30, 0);
        add(jPanel1, gridBagConstraints);

        tblDespachos.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        tblDespachos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "FECHA", "HORA", "CARA", "DESCRIPCION", "UND.", "P.UNIT.", "CANTIDAD", "IMPORTE"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblDespachos.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tblDespachos.setMinimumSize(null);
        tblDespachos.setPreferredSize(null);
        scrDespachos.setViewportView(tblDespachos);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 9;
        gridBagConstraints.ipadx = 500;
        gridBagConstraints.ipady = -200;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        add(scrDespachos, gridBagConstraints);

        btnAceptar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnAceptar.setText("Aceptar");
        btnAceptar.setEnabled(false);
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });
        jPanel2.add(btnAceptar);

        btnActualizar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });
        jPanel2.add(btnActualizar);

        btnSalir.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        jPanel2.add(btnSalir);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 8;
        gridBagConstraints.insets = new java.awt.Insets(33, 0, 0, 0);
        add(jPanel2, gridBagConstraints);

        lblAdvertencia.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        lblAdvertencia.setForeground(new java.awt.Color(255, 0, 0));
        lblAdvertencia.setText("Es importante actualizar después de generar la Nota de Despacho");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.gridwidth = 8;
        gridBagConstraints.ipady = 15;
        add(lblAdvertencia, gridBagConstraints);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        txtPlaca.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtPlaca.setPreferredSize(new java.awt.Dimension(59, 25));
        txtPlaca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPlacaActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 100;
        gridBagConstraints.ipady = 3;
        gridBagConstraints.insets = new java.awt.Insets(0, 25, 0, 0);
        jPanel3.add(txtPlaca, gridBagConstraints);

        txtDniChofer.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtDniChofer.setPreferredSize(new java.awt.Dimension(59, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 100;
        gridBagConstraints.ipady = 3;
        gridBagConstraints.insets = new java.awt.Insets(0, 25, 0, 25);
        jPanel3.add(txtDniChofer, gridBagConstraints);

        jLabel10.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel10.setText("PLACA:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 2;
        jPanel3.add(jLabel10, gridBagConstraints);

        jLabel11.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel11.setText("DNI CHOFER:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        jPanel3.add(jLabel11, gridBagConstraints);

        btnVerificar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnVerificar.setText("Verificar");
        btnVerificar.setPreferredSize(new java.awt.Dimension(101, 27));
        btnVerificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerificarActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.insets = new java.awt.Insets(0, 30, 0, 0);
        jPanel3.add(btnVerificar, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 8;
        add(jPanel3, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
        
    private void txtFechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFechaActionPerformed
            // TODO add your handling code here:
    }//GEN-LAST:event_txtFechaActionPerformed

          private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
                  int filaSeleccionada = tblDespachos.getSelectedRow();
                  if (filaSeleccionada < 0) {
                          JOptionPane.showMessageDialog(this, "Seleccione un Despacho");
                  } else {
                          int idDesp = idDespachos.get(filaSeleccionada);
                          String descripcion = tblDespachos.getValueAt(filaSeleccionada, 3).toString();
                          String unid = tblDespachos.getValueAt(filaSeleccionada, 4).toString();
                          String precioUni = tblDespachos.getValueAt(filaSeleccionada, 5).toString();
                          String cantidad = tblDespachos.getValueAt(filaSeleccionada, 6).toString();
                          String importe = tblDespachos.getValueAt(filaSeleccionada, 7).toString();
                          FrmDatosDespacho frmDatosDespacho = new FrmDatosDespacho(nroSerie, nroNota, idDesp, descripcion,
                                  unid, precioUni, cantidad, importe, dniChofer, placaVehiculo);
                          
                          btnAceptar.setEnabled(false);
                          
                          frmDatosDespacho.setVisible(true);
                          lblAdvertencia.setVisible(true);
                  }
                  System.out.println(filaSeleccionada);
          }//GEN-LAST:event_btnAceptarActionPerformed

        private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
                ActualizarDatos();
        }//GEN-LAST:event_btnActualizarActionPerformed

        private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

            int valor = JOptionPane.showConfirmDialog(this, "¿Está seguro de cerrar la aplicación?", "Advertencia", JOptionPane.YES_NO_OPTION);
            if(valor == JOptionPane.YES_OPTION){
                System.exit(0);
            }

        }//GEN-LAST:event_btnSalirActionPerformed

    private void txtPlacaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPlacaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPlacaActionPerformed

    private void btnVerificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerificarActionPerformed
        // TODO add your handling code here:
        
        dniChofer = txtDniChofer.getText();
        placaVehiculo = txtPlaca.getText();
        boolean dniBool = false;
        boolean placaBool = false;
        int cantidad = 0;
        int cantidad2 = 0;
        if(dniChofer.equals("")){
            dniChofer = "0";
        }
        ResultSet rs = bd.Consultar("SELECT count(*) FROM \"aChoferes\" WHERE \"chDNI\" = " + dniChofer + " AND \"chEstado\" = 'P'", conexion);
        ResultSet rs2 = bd.Consultar("SELECT count(*) FROM \"aVehiculos\" WHERE \"vhEstado\" = 'P' AND \"vhPlaca\" = '" + placaVehiculo + "'", conexion);
        
        try {
            rs.next();
            cantidad = Integer.parseInt(rs.getString(1));
            //---------------------------vhEstado
            rs2.next();
            cantidad2 = Integer.parseInt(rs2.getString(1));
        } catch (Exception ex) {
            System.out.println("Error al verificar dni y placa");
            ex.printStackTrace();
        }
        if(cantidad > 0){
                dniBool = true;
        }
        if(cantidad2 > 0){
                placaBool = true;
        }
        if(dniBool == false && placaBool == false){
            JOptionPane.showMessageDialog(this, "DNI y Placa no permitidas/registradas");
        }else if(dniBool == true && placaBool == true){
            JOptionPane.showMessageDialog(this, "DNI Y Placa PERMITIDAS");
            btnAceptar.setEnabled(true);
        }else if(dniBool == true){
            JOptionPane.showMessageDialog(this, "DNI permitido, Placa no permitida/registrada");
        }else{//placaBool == true
            JOptionPane.showMessageDialog(this, "DNI no permitido/registrado, placa Permitida");
        }
    }//GEN-LAST:event_btnVerificarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnVerificar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblAdvertencia;
    private javax.swing.JScrollPane scrDespachos;
    private javax.swing.JTable tblDespachos;
    private javax.swing.JTextField txtDniChofer;
    private javax.swing.JTextField txtFecha;
    public static javax.swing.JTextField txtGrifero;
    private javax.swing.JTextField txtHora;
    private javax.swing.JTextField txtNumero;
    private javax.swing.JTextField txtPlaca;
    private javax.swing.JTextField txtSerie;
    private javax.swing.JTextField txtTurno;
    // End of variables declaration//GEN-END:variables
}
