package ModuloInterfaz;

import ConexionBD.BaseDatos;
import DataSource.Conductor;
import DataSource.ConductorDataSource;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

public class PnlVentasConductor extends javax.swing.JPanel {
        
        BaseDatos bd = new BaseDatos();
        Connection conexion;
        String sql;
        String galones="", soles="", despachos=""; 
        String inicioReport="", finReport="", nombreEmpresa="";
        public PnlVentasConductor() {
                initComponents();
                
                //Comprobar la conexión a la base de datos
                conexion = bd.Conectar();
                if (conexion == null) {
                        //---------------------------Cambiar a mensaje de error---------------------------
                        int valor = JOptionPane.showConfirmDialog(this, "No se pudo conectar con la base de datos, verifique "
                                + "la conexión y reinicie el programa.", "Advertencia", JOptionPane.ERROR_MESSAGE);
                }
                
        }
        public String loadConfig(String value) {
                    Properties config = new Properties();
                    InputStream configInput = null;
                    OutputStream configOutput = null;
                    try {
                              configInput = new FileInputStream("src/data/nameProducto.properties");
                              String valor;
                              //configInput = ClassLoader.getSystemResourceAsStream("data/nameProducto.properties");
                              config.load(configInput);
                              valor = config.getProperty(value);
                              configInput.close();
                              return valor;
                    } catch (Exception e) {
                              JOptionPane.showMessageDialog(null, "Error cargando configuración\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    return null;
          }
         public void Busqueda() {
                try {
                        String fechaInicio="",fechaFin="";
                        int contador = 1;
                        if (DateInicio.getDatoFecha() == null || DateFin.getDatoFecha() == null) {
                                JOptionPane.showMessageDialog(this, "Selecciona una Fecha", "Error", JOptionPane.ERROR_MESSAGE);
                                return;
                        } else {
                                Date fecha1 = DateInicio.getDatoFecha();
                                Date fecha2 = DateFin.getDatoFecha();
                                String formato;
                                if (cbxUsarHora.isSelected()) {
                                        formato="yyyy-MM-dd HH:mm:ss";
                                        String formato2="dd/MM/yyyy HH:mm:ss";
                                        SimpleDateFormat formateador = new SimpleDateFormat(formato);
                                        SimpleDateFormat formateador2 = new SimpleDateFormat(formato2);
                                        fechaInicio = formateador.format(fecha1);
                                        fechaFin = formateador.format(fecha2);
                                        inicioReport= formateador2.format(fecha1);
                                        finReport = formateador2.format(fecha2);
                                } else {
                                        formato="yyyy-MM-dd";
                                        String formato2="dd/MM/yyyy";
                                        SimpleDateFormat formateador = new SimpleDateFormat(formato);
                                        SimpleDateFormat formateador2 = new SimpleDateFormat(formato2);
                                        fechaInicio = formateador.format(fecha1)+" 00:00:00";
                                        fechaFin = formateador.format(fecha2)+" 23:59:59";
                                        inicioReport= formateador2.format(fecha1)+" 00:00:00";
                                        finReport = formateador2.format(fecha2)+" 23:59:59";
                                }
                        }
                        DefaultTableModel modelo ;
                        //tblDatos.setModel(modelo);
                        String DNI = txtDato.getText().trim();
                        sql = "SELECT \"chNombre\", \"chDNI\", \"veProducto\", \"veVolumen\", \"veImporte\" FROM \"aChoferes\", \"aNotas\", \"aVenta\" \n"
                                + "WHERE \"ntIDVenta\"=\"veID\" AND \"chID\"=\"ntIDChofer\" AND\"ntEstado\"='E' AND \"chDNI\"="+DNI+" AND \"veHoraPC\" between '"+fechaInicio+"' AND '"+fechaFin+"'";
                        System.out.println(fechaInicio+"FEHCA INICIO");
                        System.out.println(fechaFin+"FEHCA FIN");
                        ResultSet rs = bd.Consultar(sql, conexion);
                        Object[] nombresColumnas = new Object[]{"ITEM", "NOMBRE CONDUCTOR",
                                "DNI", "PRODUCTO", "UNIDADES", "VOLUMEN", "IMPORTE"};
                        //modelo.setColumnIdentifiers(nombresColumnas);
                         modelo = new DefaultTableModel(new Object[][]{}, nombresColumnas) {
                                        @Override
                                        public boolean isCellEditable(int fila, int columna) {
                                                  return false;
                                        }
                              };
                        while (rs.next()) {
                                String producto = loadConfig(rs.getString("veProducto"));
                                Object[] fila = new Object[]{
                                        contador++,
                                        rs.getString("chNombre").trim(),
                                        rs.getString("chDNI"),
                                        producto,
                                        "GLS",
                                        String.format("%.2f", rs.getDouble("veVolumen")).replace(',', '.'),
                                        String.format("%.2f", rs.getDouble("veImporte")).replace(',', '.'),};
                                modelo.addRow(fila);
                                tblDatos.setModel(modelo);
                                tblDatos.getColumnModel().getColumn(0).setPreferredWidth(50);
                                tblDatos.getColumnModel().getColumn(1).setPreferredWidth(250);
                                 txtGalones.setText(String.format("%.2f",Suma(5)).replace(',', '.'));
                                txtSoles.setText(String.format("%.2f",Suma(6)).replace(',', '.'));
                                galones= txtGalones.getText().trim();
                                soles= txtSoles.getText().trim();
                                
                                String sql2 = "SELECT * FROM \"aDatos\"";
                                    ResultSet rs2 = bd.Consultar(sql2, conexion);
                                    while (rs2.next()) {
                                              nombreEmpresa = rs2.getString("dtNombre");
                                    }
                        }
                        return;
                } catch (Exception ex) {
                        System.out.println("Error al actualizar los despachos");
                        ex.printStackTrace();
                }
        }
         public double Suma(int columna){
                 int contar = tblDatos.getRowCount();
                 double suma=0;
                 for (int i = 0; i < contar; i++) {
                         String numero =tblDatos.getValueAt(i, columna).toString();
                         suma=suma+Double.valueOf(numero);
                 }
                 return suma;
         }
         
        public void Numeros(KeyEvent evt) {
                char caracter = evt.getKeyChar();
                if (Character.isLetter(caracter)) {
                        getToolkit().beep();
                        evt.consume();
                        //JOptionPane.showMessageDialog(rootPane, "Ingresar solo Numeros");
                }
        }

        /**
         * This method is called from within the constructor to initialize the
         * form. WARNING: Do NOT modify this code. The content of this method is
         * always regenerated by the Form Editor.
         */
        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {
                java.awt.GridBagConstraints gridBagConstraints;

                jLabel1 = new javax.swing.JLabel();
                jPanel1 = new javax.swing.JPanel();
                jLabel4 = new javax.swing.JLabel();
                jLabel3 = new javax.swing.JLabel();
                cbxUsarHora = new javax.swing.JCheckBox();
                txtDato = new javax.swing.JTextField();
                jLabel2 = new javax.swing.JLabel();
                DateInicio = new rojeru_san.componentes.RSDateChooser();
                DateFin = new rojeru_san.componentes.RSDateChooser();
                btnBuscar = new javax.swing.JButton();
                btnGenerar = new javax.swing.JButton();
                jScrollPane1 = new javax.swing.JScrollPane();
                tblDatos = new javax.swing.JTable();
                jPanel2 = new javax.swing.JPanel();
                jLabel5 = new javax.swing.JLabel();
                jLabel6 = new javax.swing.JLabel();
                jLabel8 = new javax.swing.JLabel();
                txtSoles = new javax.swing.JTextField();
                txtGalones = new javax.swing.JTextField();
                jLabel9 = new javax.swing.JLabel();

                setPreferredSize(new java.awt.Dimension(1280, 720));
                setLayout(new java.awt.GridBagLayout());

                jLabel1.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
                jLabel1.setText("VENTAS POR CONDUCTOR");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 0;
                gridBagConstraints.gridwidth = 4;
                gridBagConstraints.insets = new java.awt.Insets(20, 20, 30, 20);
                add(jLabel1, gridBagConstraints);

                jPanel1.setLayout(new java.awt.GridBagLayout());

                jLabel4.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
                jLabel4.setText("INICIO:");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 3;
                jPanel1.add(jLabel4, gridBagConstraints);

                jLabel3.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
                jLabel3.setText("FIN:");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 3;
                gridBagConstraints.gridy = 3;
                jPanel1.add(jLabel3, gridBagConstraints);

                cbxUsarHora.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
                cbxUsarHora.setText("Usar Hora (Buscar nuevamente al seleccionar)");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 1;
                gridBagConstraints.gridwidth = 7;
                gridBagConstraints.insets = new java.awt.Insets(0, 0, 3, 0);
                jPanel1.add(cbxUsarHora, gridBagConstraints);

                txtDato.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
                txtDato.setHorizontalAlignment(javax.swing.JTextField.LEFT);
                txtDato.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                txtDato.addKeyListener(new java.awt.event.KeyAdapter() {
                        public void keyTyped(java.awt.event.KeyEvent evt) {
                                txtDatoKeyTyped(evt);
                        }
                });
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 3;
                gridBagConstraints.gridy = 0;
                gridBagConstraints.ipadx = 100;
                gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
                jPanel1.add(txtDato, gridBagConstraints);

                jLabel2.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
                jLabel2.setText("INGRESE DNI:");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 2;
                gridBagConstraints.gridy = 0;
                gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
                jPanel1.add(jLabel2, gridBagConstraints);

                DateInicio.setFormatoFecha("yyyy/MM/dd HH:mm:ss");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 1;
                gridBagConstraints.gridy = 3;
                gridBagConstraints.gridwidth = 2;
                gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                gridBagConstraints.insets = new java.awt.Insets(0, 30, 0, 0);
                jPanel1.add(DateInicio, gridBagConstraints);

                DateFin.setFormatoFecha("yyyy/MM/dd HH:mm:ss");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 6;
                gridBagConstraints.gridy = 3;
                gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
                jPanel1.add(DateFin, gridBagConstraints);

                btnBuscar.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
                btnBuscar.setText("BUSCAR");
                btnBuscar.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnBuscarActionPerformed(evt);
                        }
                });
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 7;
                gridBagConstraints.gridwidth = 7;
                gridBagConstraints.gridheight = 2;
                gridBagConstraints.insets = new java.awt.Insets(20, 0, 0, 150);
                jPanel1.add(btnBuscar, gridBagConstraints);

                btnGenerar.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
                btnGenerar.setText("GENERAR");
                btnGenerar.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnGenerarActionPerformed(evt);
                        }
                });
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 7;
                gridBagConstraints.gridwidth = 7;
                gridBagConstraints.insets = new java.awt.Insets(20, 150, 0, 0);
                jPanel1.add(btnGenerar, gridBagConstraints);

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 1;
                gridBagConstraints.gridwidth = 4;
                add(jPanel1, gridBagConstraints);

                tblDatos.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
                tblDatos.setModel(new javax.swing.table.DefaultTableModel(
                        new Object [][] {
                                {null, null, null, null, null, null, null},
                                {null, null, null, null, null, null, null},
                                {null, null, null, null, null, null, null},
                                {null, null, null, null, null, null, null},
                                {null, null, null, null, null, null, null},
                                {null, null, null, null, null, null, null},
                                {null, null, null, null, null, null, null},
                                {null, null, null, null, null, null, null},
                                {null, null, null, null, null, null, null},
                                {null, null, null, null, null, null, null},
                                {null, null, null, null, null, null, null},
                                {null, null, null, null, null, null, null},
                                {null, null, null, null, null, null, null},
                                {null, null, null, null, null, null, null},
                                {null, null, null, null, null, null, null}
                        },
                        new String [] {
                                "ITEM", "NOMBRE CONDUCTOR", "DNI", "PRODUCTO", "UNIDADES", "VOLUMEN", "IMPORTE"
                        }
                ) {
                        Class[] types = new Class [] {
                                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Double.class, java.lang.Double.class
                        };
                        boolean[] canEdit = new boolean [] {
                                false, false, false, false, false, false, false
                        };

                        public Class getColumnClass(int columnIndex) {
                                return types [columnIndex];
                        }

                        public boolean isCellEditable(int rowIndex, int columnIndex) {
                                return canEdit [columnIndex];
                        }
                });
                jScrollPane1.setViewportView(tblDatos);
                if (tblDatos.getColumnModel().getColumnCount() > 0) {
                        tblDatos.getColumnModel().getColumn(0).setResizable(false);
                        tblDatos.getColumnModel().getColumn(0).setPreferredWidth(50);
                        tblDatos.getColumnModel().getColumn(1).setPreferredWidth(300);
                }

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 2;
                gridBagConstraints.gridwidth = 4;
                gridBagConstraints.ipadx = 300;
                gridBagConstraints.ipady = -225;
                gridBagConstraints.insets = new java.awt.Insets(25, 0, 0, 0);
                add(jScrollPane1, gridBagConstraints);

                jLabel5.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
                jLabel5.setText("TOTALES VOLUMEN");

                jLabel6.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
                jLabel6.setText("TOTALES IMPORTE");

                jLabel8.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
                jLabel8.setText("GALONES");

                txtSoles.setEditable(false);
                txtSoles.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N

                txtGalones.setEditable(false);
                txtGalones.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N

                jLabel9.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
                jLabel9.setText("SOLES");

                javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
                jPanel2.setLayout(jPanel2Layout);
                jPanel2Layout.setHorizontalGroup(
                        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(30, 30, 30)
                                .addComponent(txtGalones, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(14, 14, 14)
                                .addComponent(jLabel8))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jLabel6)
                                .addGap(33, 33, 33)
                                .addComponent(txtSoles, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(14, 14, 14)
                                .addComponent(jLabel9))
                );
                jPanel2Layout.setVerticalGroup(
                        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtGalones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGap(3, 3, 3)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel5)
                                                        .addComponent(jLabel8))))
                                .addGap(15, 15, 15)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtSoles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGap(3, 3, 3)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel6)
                                                        .addComponent(jLabel9)))))
                );

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 3;
                gridBagConstraints.gridwidth = 4;
                add(jPanel2, gridBagConstraints);
        }// </editor-fold>//GEN-END:initComponents

        private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
                Busqueda();
              


        }//GEN-LAST:event_btnBuscarActionPerformed

        private void txtDatoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDatoKeyTyped
                Numeros(evt);  
        }//GEN-LAST:event_txtDatoKeyTyped

        private void btnGenerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarActionPerformed
                InputStream direccion = null;
                JasperPrint print = null;
                ConductorDataSource dataSource = new ConductorDataSource();

                for (int i = 0; i < tblDatos.getRowCount(); i++) { // Iterena cada fila de la tabla
                        Conductor em;// Instaciamos la clase CONDUCTOR
                        em = new Conductor(tblDatos.getValueAt(i, 0).toString(), tblDatos.getValueAt(i, 1).toString(), //Tomamos de la tabla el valor de cada columna y creamos un objeto empleado
                                tblDatos.getValueAt(i, 2).toString(), tblDatos.getValueAt(i, 3).toString(), tblDatos.getValueAt(i, 4).toString(), 
                                tblDatos.getValueAt(i, 5).toString(), tblDatos.getValueAt(i, 6).toString());
                        // lista.add(em); //Agregamos el objeto empleado a la lista
                        dataSource.addConductor(em);
                        if (i+1 == tblDatos.getRowCount()) {
                                despachos = String.valueOf(tblDatos.getValueAt(i, 0));
                        }
                }
                try {
                        //direccion = new FileInputStream("src/Reportes/Reporte_por_Conductor.jrxml");
                       direccion = ClassLoader.getSystemResourceAsStream("Reportes/Reporte_por_Conductor.jrxml");
                } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, "Error al leer el fichero de carga" + ex.getMessage());
                }
                try {
                        JasperDesign jasperDesign = JRXmlLoader.load(direccion);
                        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                        Map parameters = new HashMap();
                        parameters.put("fechaInicio", inicioReport);
                        parameters.put("fechaFin", finReport);
                        parameters.put("galones", galones);
                        parameters.put("soles", soles);
                        parameters.put("despachos", despachos);
                        parameters.put("nombre", nombreEmpresa);
                        print = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
                        JasperViewer jasperViewer = new JasperViewer(print, false);
                        jasperViewer.setVisible(true);
                        jasperViewer.setIconImage(new ImageIcon(getClass().getResource("/image/icon_report.png")).getImage());
                        jasperViewer.setTitle("Reporte Ventas por Conductor");

                } catch (Exception e) {
                        e.printStackTrace();
                }
        }//GEN-LAST:event_btnGenerarActionPerformed


        // Variables declaration - do not modify//GEN-BEGIN:variables
        private rojeru_san.componentes.RSDateChooser DateFin;
        private rojeru_san.componentes.RSDateChooser DateInicio;
        private javax.swing.JButton btnBuscar;
        private javax.swing.JButton btnGenerar;
        private javax.swing.JCheckBox cbxUsarHora;
        private javax.swing.JLabel jLabel1;
        private javax.swing.JLabel jLabel2;
        private javax.swing.JLabel jLabel3;
        private javax.swing.JLabel jLabel4;
        private javax.swing.JLabel jLabel5;
        private javax.swing.JLabel jLabel6;
        private javax.swing.JLabel jLabel8;
        private javax.swing.JLabel jLabel9;
        private javax.swing.JPanel jPanel1;
        private javax.swing.JPanel jPanel2;
        private javax.swing.JScrollPane jScrollPane1;
        private javax.swing.JTable tblDatos;
        private javax.swing.JTextField txtDato;
        private javax.swing.JTextField txtGalones;
        private javax.swing.JTextField txtSoles;
        // End of variables declaration//GEN-END:variables
}
