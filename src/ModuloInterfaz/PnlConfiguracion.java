package ModuloInterfaz;

import ConexionBD.BaseDatos;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import javax.swing.JOptionPane;
import java.util.Properties;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;

public class PnlConfiguracion extends javax.swing.JPanel {

        public String loadConfig(String value) {
                Properties config = new Properties();
                InputStream configInput = null;
                OutputStream configOutput = null;
                try {
                        configInput = new FileInputStream("src/data/config.properties");
                        String valor;
                        //configInput = ClassLoader.getSystemResourceAsStream("data/config.properties");
                        config.load(configInput);
                        valor = config.getProperty(value);
                        configInput.close();
                        return valor;
                } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Error cargando configuración\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
                return null;
        }

        public PnlConfiguracion() {
                initComponents();

        }

        public void obtener() {
                txtDireccion.setText(loadConfig("direccion"));
                txtPuerto.setText(loadConfig("puerto"));
                txtBaseDatos.setText(loadConfig("baseDatos"));
                txtUsuario.setText(loadConfig("usuario"));
                txtClave.setText(loadConfig("clave"));
        }


        public void escribirPropiedades() {
                Properties config = new Properties();
                InputStream configInput = null;
                OutputStream configOutput = null;
                try {
                        configOutput = new FileOutputStream("src/data/config.properties");
                        //configInput = ClassLoader.getSystemResourceAsStream("data/config.properties");
                        config.setProperty("direccion", txtDireccion.getText().trim());
                        config.setProperty("puerto", txtPuerto.getText().trim());
                        config.setProperty("baseDatos", txtBaseDatos.getText().trim());
                        config.setProperty("usuario", txtUsuario.getText().trim());
                        config.setProperty("clave", txtClave.getText().trim());
                        config.store(configOutput, "Cambios realizados");
                } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Error guardando configuración\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
        }

        @SuppressWarnings("unchecked")
          // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
          private void initComponents() {
                    java.awt.GridBagConstraints gridBagConstraints;

                    jLabel6 = new javax.swing.JLabel();
                    jPanel1 = new javax.swing.JPanel();
                    jLabel1 = new javax.swing.JLabel();
                    txtDireccion = new javax.swing.JTextField();
                    jLabel2 = new javax.swing.JLabel();
                    txtPuerto = new javax.swing.JTextField();
                    jLabel3 = new javax.swing.JLabel();
                    txtBaseDatos = new javax.swing.JTextField();
                    jLabel4 = new javax.swing.JLabel();
                    txtUsuario = new javax.swing.JTextField();
                    jLabel5 = new javax.swing.JLabel();
                    txtClave = new javax.swing.JPasswordField();
                    jPanel2 = new javax.swing.JPanel();
                    btnObtener = new javax.swing.JButton();
                    btnComprobar = new javax.swing.JButton();
                    btnGuardar = new javax.swing.JButton();

                    setLayout(new java.awt.GridBagLayout());

                    jLabel6.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
                    jLabel6.setText("CONEXION A BASE DE DATOS");
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = 0;
                    gridBagConstraints.gridwidth = 2;
                    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                    add(jLabel6, gridBagConstraints);

                    jLabel1.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
                    jLabel1.setText("Direccion:");

                    txtDireccion.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N

                    jLabel2.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
                    jLabel2.setText("Puerto:");

                    txtPuerto.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N

                    jLabel3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
                    jLabel3.setText("Base de Datos:");

                    txtBaseDatos.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N

                    jLabel4.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
                    jLabel4.setText("Usuario:");

                    txtUsuario.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N

                    jLabel5.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
                    jLabel5.setText("Clave:");

                    txtClave.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N

                    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
                    jPanel1.setLayout(jPanel1Layout);
                    jPanel1Layout.setHorizontalGroup(
                              jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                              .addGap(0, 392, Short.MAX_VALUE)
                              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                  .addGap(31, 31, 31)
                                                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(jLabel1)
                                                            .addComponent(jLabel2)
                                                            .addComponent(jLabel3)
                                                            .addComponent(jLabel4)
                                                            .addComponent(jLabel5))
                                                  .addGap(58, 58, 58)
                                                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                            .addComponent(txtDireccion)
                                                            .addComponent(txtPuerto)
                                                            .addComponent(txtBaseDatos)
                                                            .addComponent(txtUsuario)
                                                            .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                  .addContainerGap(54, Short.MAX_VALUE)))
                    );
                    jPanel1Layout.setVerticalGroup(
                              jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                              .addGap(0, 332, Short.MAX_VALUE)
                              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                  .addGap(42, 42, 42)
                                                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                            .addComponent(jLabel1)
                                                            .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                  .addGap(18, 18, 18)
                                                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                            .addComponent(jLabel2)
                                                            .addComponent(txtPuerto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                  .addGap(18, 18, 18)
                                                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                            .addComponent(jLabel3)
                                                            .addComponent(txtBaseDatos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                  .addGap(18, 18, 18)
                                                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                            .addComponent(jLabel4)
                                                            .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                  .addGap(18, 18, 18)
                                                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                            .addComponent(jLabel5)
                                                            .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                  .addContainerGap(73, Short.MAX_VALUE)))
                    );

                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = 1;
                    gridBagConstraints.gridwidth = 2;
                    gridBagConstraints.ipadx = 22;
                    gridBagConstraints.ipady = 31;
                    add(jPanel1, gridBagConstraints);

                    jPanel2.setLayout(new java.awt.GridBagLayout());

                    btnObtener.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
                    btnObtener.setText("OBTENER");
                    btnObtener.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        btnObtenerActionPerformed(evt);
                              }
                    });
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = 0;
                    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                    gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 30);
                    jPanel2.add(btnObtener, gridBagConstraints);

                    btnComprobar.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
                    btnComprobar.setText("COMPROBAR");
                    btnComprobar.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        btnComprobarActionPerformed(evt);
                              }
                    });
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 1;
                    gridBagConstraints.gridy = 0;
                    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                    jPanel2.add(btnComprobar, gridBagConstraints);

                    btnGuardar.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
                    btnGuardar.setText("GUARDAR");
                    btnGuardar.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        btnGuardarActionPerformed(evt);
                              }
                    });
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 2;
                    gridBagConstraints.gridy = 0;
                    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
                    gridBagConstraints.insets = new java.awt.Insets(0, 30, 0, 0);
                    jPanel2.add(btnGuardar, gridBagConstraints);

                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = 2;
                    gridBagConstraints.gridwidth = 2;
                    add(jPanel2, gridBagConstraints);
          }// </editor-fold>//GEN-END:initComponents

        private void btnComprobarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnComprobarActionPerformed

                 String direccion = "", puerto = "", baseDatos = "", usuario = "", clave = "", driver = "", url = "";
                 direccion = txtDireccion.getText().trim();
                 puerto = txtPuerto.getText().trim();
                 baseDatos = txtBaseDatos.getText().trim();
                 usuario = txtUsuario.getText().trim();
                 clave = txtClave.getText().trim();
                 driver = "org.postgresql.Driver";
                 url = "jdbc:postgresql://" + direccion + ":" + puerto + "/" + baseDatos;
                 try {
                        Class.forName(driver);
                        Connection conexion = DriverManager.getConnection(url, usuario, clave);
                        if (conexion != null) {
                                JOptionPane.showMessageDialog(null, "Conexión Exitosa", "Conexión", 1);
                        }
                } catch (Exception e) {
                        JOptionPane.showMessageDialog(  this, "No se pudo conectar con la base de datos, verifique "
                                        + "la conexión y reinicie el programa.", "Advertencia", JOptionPane.ERROR_MESSAGE);
                }

        }//GEN-LAST:event_btnComprobarActionPerformed

        private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
                escribirPropiedades();
        }//GEN-LAST:event_btnGuardarActionPerformed

        private void btnObtenerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnObtenerActionPerformed
                obtener();
        }//GEN-LAST:event_btnObtenerActionPerformed


          // Variables declaration - do not modify//GEN-BEGIN:variables
          private javax.swing.JButton btnComprobar;
          private javax.swing.JButton btnGuardar;
          private javax.swing.JButton btnObtener;
          private javax.swing.JLabel jLabel1;
          private javax.swing.JLabel jLabel2;
          private javax.swing.JLabel jLabel3;
          private javax.swing.JLabel jLabel4;
          private javax.swing.JLabel jLabel5;
          private javax.swing.JLabel jLabel6;
          private javax.swing.JPanel jPanel1;
          private javax.swing.JPanel jPanel2;
          private javax.swing.JTextField txtBaseDatos;
          private javax.swing.JPasswordField txtClave;
          private javax.swing.JTextField txtDireccion;
          private javax.swing.JTextField txtPuerto;
          private javax.swing.JTextField txtUsuario;
          // End of variables declaration//GEN-END:variables
}
