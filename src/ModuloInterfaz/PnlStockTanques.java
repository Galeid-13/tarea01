/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloInterfaz;

/**
 *
 * @author Acer
 */
public class PnlStockTanques extends javax.swing.JPanel {

        /**
         * Creates new form PnlStockTanques
         */
        public PnlStockTanques() {
                initComponents();
        }

        /**
         * This method is called from within the constructor to initialize the
         * form. WARNING: Do NOT modify this code. The content of this method is
         * always regenerated by the Form Editor.
         */
        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {
                java.awt.GridBagConstraints gridBagConstraints;

                jLabel1 = new javax.swing.JLabel();
                tblDespachos = new javax.swing.JScrollPane();
                jTable1 = new javax.swing.JTable();
                jLabel2 = new javax.swing.JLabel();
                txtFechaDescarga = new javax.swing.JTextField();
                jLabel3 = new javax.swing.JLabel();
                txtHoraDescarga = new javax.swing.JTextField();
                jLabel4 = new javax.swing.JLabel();
                txtEncargadp = new javax.swing.JTextField();
                jLabel5 = new javax.swing.JLabel();
                txtProducto = new javax.swing.JTextField();
                jLabel6 = new javax.swing.JLabel();
                txtProveedor = new javax.swing.JTextField();
                jLabel7 = new javax.swing.JLabel();
                txtNumeroGuia = new javax.swing.JTextField();
                jLabel8 = new javax.swing.JLabel();
                txtFactura = new javax.swing.JTextField();
                jLabel9 = new javax.swing.JLabel();
                txtVolumen = new javax.swing.JTextField();
                jLabel10 = new javax.swing.JLabel();
                txtTanque = new javax.swing.JTextField();
                btnAplicar = new javax.swing.JButton();
                btnFinalizar = new javax.swing.JButton();

                setLayout(new java.awt.GridBagLayout());

                jLabel1.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
                jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                jLabel1.setText("ACTUALIZAR STOCK DE TANQUES");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 2;
                gridBagConstraints.gridy = 0;
                gridBagConstraints.gridwidth = java.awt.GridBagConstraints.RELATIVE;
                gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
                gridBagConstraints.insets = new java.awt.Insets(0, 0, 20, 0);
                add(jLabel1, gridBagConstraints);

                jTable1.setModel(new javax.swing.table.DefaultTableModel(
                        new Object [][] {
                                {null, null, null, null},
                                {null, null, null, null},
                                {null, null, null, null},
                                {null, null, null, null}
                        },
                        new String [] {
                                "Title 1", "Title 2", "Title 3", "Title 4"
                        }
                ));
                tblDespachos.setViewportView(jTable1);

                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 0;
                gridBagConstraints.gridy = 16;
                gridBagConstraints.gridwidth = 9;
                gridBagConstraints.ipady = -300;
                gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
                add(tblDespachos, gridBagConstraints);

                jLabel2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                jLabel2.setText("TANQUE");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 2;
                gridBagConstraints.gridy = 17;
                gridBagConstraints.insets = new java.awt.Insets(20, 20, 20, 20);
                add(jLabel2, gridBagConstraints);

                txtFechaDescarga.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 3;
                gridBagConstraints.gridy = 22;
                gridBagConstraints.ipadx = 100;
                add(txtFechaDescarga, gridBagConstraints);

                jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                jLabel3.setText("PRODUCTO");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 5;
                gridBagConstraints.gridy = 17;
                gridBagConstraints.insets = new java.awt.Insets(20, 20, 20, 20);
                add(jLabel3, gridBagConstraints);

                txtHoraDescarga.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                txtHoraDescarga.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                txtHoraDescargaActionPerformed(evt);
                        }
                });
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 6;
                gridBagConstraints.gridy = 22;
                gridBagConstraints.ipadx = 100;
                add(txtHoraDescarga, gridBagConstraints);

                jLabel4.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                jLabel4.setText("ENCARGADO DESCARGA");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 2;
                gridBagConstraints.gridy = 23;
                gridBagConstraints.insets = new java.awt.Insets(20, 20, 20, 20);
                add(jLabel4, gridBagConstraints);

                txtEncargadp.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 3;
                gridBagConstraints.gridy = 23;
                gridBagConstraints.gridwidth = 4;
                gridBagConstraints.ipadx = 400;
                add(txtEncargadp, gridBagConstraints);

                jLabel5.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                jLabel5.setText("HORA DE DESCARGA");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 5;
                gridBagConstraints.gridy = 22;
                gridBagConstraints.insets = new java.awt.Insets(20, 20, 20, 20);
                add(jLabel5, gridBagConstraints);

                txtProducto.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 6;
                gridBagConstraints.gridy = 17;
                gridBagConstraints.ipadx = 100;
                add(txtProducto, gridBagConstraints);

                jLabel6.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                jLabel6.setText("PROVEEDOR");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 2;
                gridBagConstraints.gridy = 24;
                gridBagConstraints.insets = new java.awt.Insets(20, 20, 20, 20);
                add(jLabel6, gridBagConstraints);

                txtProveedor.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 3;
                gridBagConstraints.gridy = 24;
                gridBagConstraints.gridwidth = java.awt.GridBagConstraints.RELATIVE;
                gridBagConstraints.ipadx = 400;
                add(txtProveedor, gridBagConstraints);

                jLabel7.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                jLabel7.setText("NUMERO DE GUIA DE REMISION");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 2;
                gridBagConstraints.gridy = 26;
                gridBagConstraints.insets = new java.awt.Insets(20, 20, 20, 20);
                add(jLabel7, gridBagConstraints);

                txtNumeroGuia.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 3;
                gridBagConstraints.gridy = 26;
                gridBagConstraints.gridwidth = java.awt.GridBagConstraints.RELATIVE;
                gridBagConstraints.ipadx = 400;
                add(txtNumeroGuia, gridBagConstraints);

                jLabel8.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                jLabel8.setText("NUMERO DE FACTURA");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 2;
                gridBagConstraints.gridy = 27;
                gridBagConstraints.insets = new java.awt.Insets(20, 20, 20, 20);
                add(jLabel8, gridBagConstraints);

                txtFactura.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 3;
                gridBagConstraints.gridy = 27;
                gridBagConstraints.gridwidth = java.awt.GridBagConstraints.RELATIVE;
                gridBagConstraints.ipadx = 400;
                add(txtFactura, gridBagConstraints);

                jLabel9.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                jLabel9.setText("VOLUMEN DESCARGADO");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 2;
                gridBagConstraints.gridy = 28;
                gridBagConstraints.insets = new java.awt.Insets(20, 20, 20, 20);
                add(jLabel9, gridBagConstraints);

                txtVolumen.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 3;
                gridBagConstraints.gridy = 28;
                gridBagConstraints.gridwidth = java.awt.GridBagConstraints.RELATIVE;
                gridBagConstraints.ipadx = 400;
                add(txtVolumen, gridBagConstraints);

                jLabel10.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                jLabel10.setText("FECHA DESCARGA");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 2;
                gridBagConstraints.gridy = 22;
                gridBagConstraints.insets = new java.awt.Insets(20, 20, 20, 20);
                add(jLabel10, gridBagConstraints);

                txtTanque.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 3;
                gridBagConstraints.gridy = 17;
                gridBagConstraints.ipadx = 100;
                add(txtTanque, gridBagConstraints);

                btnAplicar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                btnAplicar.setText("APLICAR");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 2;
                gridBagConstraints.gridy = 29;
                gridBagConstraints.gridwidth = java.awt.GridBagConstraints.RELATIVE;
                gridBagConstraints.insets = new java.awt.Insets(20, 0, 0, 200);
                add(btnAplicar, gridBagConstraints);

                btnFinalizar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                btnFinalizar.setText("FINALIZAR");
                gridBagConstraints = new java.awt.GridBagConstraints();
                gridBagConstraints.gridx = 2;
                gridBagConstraints.gridy = 29;
                gridBagConstraints.gridwidth = java.awt.GridBagConstraints.RELATIVE;
                gridBagConstraints.insets = new java.awt.Insets(20, 200, 0, 0);
                add(btnFinalizar, gridBagConstraints);
        }// </editor-fold>//GEN-END:initComponents

        private void txtHoraDescargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHoraDescargaActionPerformed
                // TODO add your handling code here:
        }//GEN-LAST:event_txtHoraDescargaActionPerformed


        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JButton btnAplicar;
        private javax.swing.JButton btnFinalizar;
        private javax.swing.JLabel jLabel1;
        private javax.swing.JLabel jLabel10;
        private javax.swing.JLabel jLabel2;
        private javax.swing.JLabel jLabel3;
        private javax.swing.JLabel jLabel4;
        private javax.swing.JLabel jLabel5;
        private javax.swing.JLabel jLabel6;
        private javax.swing.JLabel jLabel7;
        private javax.swing.JLabel jLabel8;
        private javax.swing.JLabel jLabel9;
        private javax.swing.JTable jTable1;
        private javax.swing.JScrollPane tblDespachos;
        private javax.swing.JTextField txtEncargadp;
        private javax.swing.JTextField txtFactura;
        private javax.swing.JTextField txtFechaDescarga;
        private javax.swing.JTextField txtHoraDescarga;
        private javax.swing.JTextField txtNumeroGuia;
        private javax.swing.JTextField txtProducto;
        private javax.swing.JTextField txtProveedor;
        private javax.swing.JTextField txtTanque;
        private javax.swing.JTextField txtVolumen;
        // End of variables declaration//GEN-END:variables
}
