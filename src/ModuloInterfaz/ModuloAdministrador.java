/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloInterfaz;

import ConexionBD.BaseDatos;
import ConexionBD.Usuario;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Acer
 */
public class ModuloAdministrador extends javax.swing.JFrame {

        PnlGenerarNota pnlGenerarNota = new PnlGenerarNota();
        PnlReimprimirNota pnlReimprimirNota = new PnlReimprimirNota();
        PnlAnularNota pnlAnularNota = new PnlAnularNota();
        PnlTurno pnlTurno = new PnlTurno();
        PnlInicio pnlInicio = new PnlInicio();
        PnlVentasConductor pnlVentasConductor = new PnlVentasConductor();
        PnlVentasVehiculo pnlVentasVehiculo = new PnlVentasVehiculo();
        PnlVentasCara pnlVentasCara = new PnlVentasCara();
        PnlVentasManguera pnlVentasManguera = new PnlVentasManguera();
        PnlVentasProducto pnlVentasProducto = new PnlVentasProducto();
        PnlVentasTiempo pnlVentasTiempo = new PnlVentasTiempo();
        PnlConfiguracion pnlConfiguracion = new PnlConfiguracion();
        PnlDatosGenerales pnlDatosGenerales = new PnlDatosGenerales();
        PnlCambioPrecios pnlCambioprecio = new PnlCambioPrecios();


        FrmLogeo frmLogeo=new FrmLogeo();
        
        PnlVehiculosConfiguracion pnlVehiculosConfiguracion = new PnlVehiculosConfiguracion();
        PnlChoferConfiguracion pnlChoferConfiguracion = new PnlChoferConfiguracion();
        PnlUsuariosConfiguracion pnlUsuariosConfiguracion = new PnlUsuariosConfiguracion();

        BaseDatos bd = new BaseDatos();
        Connection conexion;
        
        public ModuloAdministrador() {
                initComponents();
                this.setTitle("Admin");
                this.setIconImage(new ImageIcon(getClass().getResource("/image/Proyecto.png")).getImage());
                
                this.setLocationRelativeTo(null);
            contenedorAdmin.add(pnlGenerarNota);

            conexion = bd.Conectar();
            if(conexion == null){
                //---------------------------Cambiar a mensaje de error
                int valor = JOptionPane.showConfirmDialog(this, "No se pudo conectar con la base de datos, verifique "
                        + "la conexión y reinicie el programa.", "Advertencia", JOptionPane.ERROR_MESSAGE);
            }
pnlGenerarNota.ActualizarDatos();
            VisibilidadPaneles(false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false);

            Cerrar();
        }
        
        //Metodo para la visualizacion de los paneles (Solo un parámetro debe ser "true")
        public void VisibilidadPaneles(boolean pi,boolean gn, boolean rn, boolean an, boolean ct, boolean vc, 
                boolean vv, boolean vca, boolean vm, boolean vp, boolean vt, boolean dg, boolean cc, boolean chc,
                boolean vhc, boolean usc, boolean cp){
            pnlInicio.setVisible(pi);
            pnlGenerarNota.setVisible(gn);
            pnlReimprimirNota.setVisible(rn);
            pnlAnularNota.setVisible(an);
            pnlTurno.setVisible(ct);
            pnlVentasConductor.setVisible(vc);
            pnlVentasVehiculo.setVisible(vv);
            pnlVentasCara.setVisible(vca);
            pnlVentasManguera.setVisible(vm);
            pnlVentasProducto.setVisible(vp);
            pnlVentasTiempo.setVisible(vt);
            pnlConfiguracion.setVisible(cc);
            pnlDatosGenerales.setVisible(dg);
            pnlCambioprecio.setVisible(cp);
            
            pnlChoferConfiguracion.setVisible(chc);
            pnlVehiculosConfiguracion.setVisible(vhc);
            pnlUsuariosConfiguracion.setVisible(usc);
        }
        
        //Metodo para detectar cuando se cierre la ventana
        public void Cerrar(){
            try {
                this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                addWindowListener(new WindowAdapter(){
                    public void windowClosing (WindowEvent e){
                        CerrarAplicacion();
                    }
                });
            } catch (Exception e) {
                System.out.println("Error" + e);
            }
        }
        
        public void CerrarAplicacion(){
            int valor = JOptionPane.showConfirmDialog(this, "¿Está seguro de cerrar la aplicación?", "Advertencia", JOptionPane.YES_NO_OPTION);
            if(valor == JOptionPane.YES_OPTION){
                System.exit(0);
            }
        }
        public void CerrarSesion(){
            int input = JOptionPane.showConfirmDialog(null, 
                "¿Esta seguro que quiere Cerrar Sesión?", "Confirmacion",JOptionPane.YES_NO_OPTION);
                // 0=yes, 1=no, 2=cancel
                if (input==0) {
                    Usuario user = new Usuario();
                    //CerrarAplicacion();
                    user.setUsID(0);
                    user.setUsNombre("");
                    user.setUsDNI(0);
                    user.setUsTipo("");
                    user.setUsNickname("");
                    user.setUsPassword("");
                    frmLogeo.setVisible(true);
                    this.setVisible(false);
                } 
        }
        /**
         * This method is called from within the constructor to initialize the
         * form. WARNING: Do NOT modify this code. The content of this method is
         * always regenerated by the Form Editor.
         */
        @SuppressWarnings("unchecked")
          // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
          private void initComponents() {

                    contenedorAdmin = new javax.swing.JPanel();
                    jMenuBar1 = new javax.swing.JMenuBar();
                    jMenu1 = new javax.swing.JMenu();
                    mnSalir = new javax.swing.JMenuItem();
                    mnCerrarSesion = new javax.swing.JMenuItem();
                    jMenu2 = new javax.swing.JMenu();
                    mnGenerarNota = new javax.swing.JMenuItem();
                    mnReimprimirNota = new javax.swing.JMenuItem();
                    mnAnularNota = new javax.swing.JMenuItem();
                    jMenu3 = new javax.swing.JMenu();
                    mnCambioTurno = new javax.swing.JMenuItem();
                    jMenu4 = new javax.swing.JMenu();
                    mnReporteTiempo = new javax.swing.JMenuItem();
                    mnVehiculo = new javax.swing.JMenuItem();
                    mnConductor = new javax.swing.JMenuItem();
                    mnCara = new javax.swing.JMenuItem();
                    mnManguera = new javax.swing.JMenuItem();
                    mnProducto = new javax.swing.JMenuItem();
                    jMenu6 = new javax.swing.JMenu();
                    mnDataBase = new javax.swing.JMenuItem();
                    mnDatosGenerales = new javax.swing.JMenuItem();
                    mnCambioPrecio = new javax.swing.JMenuItem();
                    jMenu7 = new javax.swing.JMenu();
                    jMenuItem2 = new javax.swing.JMenuItem();
                    jMenuItem3 = new javax.swing.JMenuItem();
                    jMenuItem4 = new javax.swing.JMenuItem();

                    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                    setPreferredSize(new java.awt.Dimension(1280, 720));

                    contenedorAdmin.setLayout(new java.awt.BorderLayout());
                    getContentPane().add(contenedorAdmin, java.awt.BorderLayout.CENTER);

                    jMenu1.setText("Archivo");

                    mnSalir.setText("Salir");
                    mnSalir.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnSalirActionPerformed(evt);
                              }
                    });
                    jMenu1.add(mnSalir);

                    mnCerrarSesion.setText("Cerrar Sesion");
                    mnCerrarSesion.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnCerrarSesionActionPerformed(evt);
                              }
                    });
                    jMenu1.add(mnCerrarSesion);

                    jMenuBar1.add(jMenu1);

                    jMenu2.setText("Despachos");

                    mnGenerarNota.setText("Generar Nota");
                    mnGenerarNota.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnGenerarNotaActionPerformed(evt);
                              }
                    });
                    jMenu2.add(mnGenerarNota);

                    mnReimprimirNota.setText("Reimprimir Nota");
                    mnReimprimirNota.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnReimprimirNotaActionPerformed(evt);
                              }
                    });
                    jMenu2.add(mnReimprimirNota);

                    mnAnularNota.setText("Anular Nota");
                    mnAnularNota.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnAnularNotaActionPerformed(evt);
                              }
                    });
                    jMenu2.add(mnAnularNota);

                    jMenuBar1.add(jMenu2);

                    jMenu3.setText("Turno");

                    mnCambioTurno.setText("Cambio Turno");
                    mnCambioTurno.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnCambioTurnoActionPerformed(evt);
                              }
                    });
                    jMenu3.add(mnCambioTurno);

                    jMenuBar1.add(jMenu3);

                    jMenu4.setText("Reportes");

                    mnReporteTiempo.setText("Por Tiempo");
                    mnReporteTiempo.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnReporteTiempoActionPerformed(evt);
                              }
                    });
                    jMenu4.add(mnReporteTiempo);

                    mnVehiculo.setText("Vehiculos");
                    mnVehiculo.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnVehiculoActionPerformed(evt);
                              }
                    });
                    jMenu4.add(mnVehiculo);

                    mnConductor.setText("Conductor");
                    mnConductor.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnConductorActionPerformed(evt);
                              }
                    });
                    jMenu4.add(mnConductor);

                    mnCara.setText("Cara");
                    mnCara.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnCaraActionPerformed(evt);
                              }
                    });
                    jMenu4.add(mnCara);

                    mnManguera.setText("Manguera");
                    mnManguera.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnMangueraActionPerformed(evt);
                              }
                    });
                    jMenu4.add(mnManguera);

                    mnProducto.setText("Producto");
                    mnProducto.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnProductoActionPerformed(evt);
                              }
                    });
                    jMenu4.add(mnProducto);

                    jMenuBar1.add(jMenu4);

                    jMenu6.setText("Configurar");

                    mnDataBase.setText("Conexion Base de Datos");
                    mnDataBase.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnDataBaseActionPerformed(evt);
                              }
                    });
                    jMenu6.add(mnDataBase);

                    mnDatosGenerales.setText("Datos Generales");
                    mnDatosGenerales.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnDatosGeneralesActionPerformed(evt);
                              }
                    });
                    jMenu6.add(mnDatosGenerales);

                    mnCambioPrecio.setText("Cambio Precio");
                    mnCambioPrecio.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        mnCambioPrecioActionPerformed(evt);
                              }
                    });
                    jMenu6.add(mnCambioPrecio);

                    jMenuBar1.add(jMenu6);

                    jMenu7.setText("Gestión");

                    jMenuItem2.setText("Choferes");
                    jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        jMenuItem2ActionPerformed(evt);
                              }
                    });
                    jMenu7.add(jMenuItem2);

                    jMenuItem3.setText("Vehiculos");
                    jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        jMenuItem3ActionPerformed(evt);
                              }
                    });
                    jMenu7.add(jMenuItem3);

                    jMenuItem4.setText("Usuarios");
                    jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        jMenuItem4ActionPerformed(evt);
                              }
                    });
                    jMenu7.add(jMenuItem4);

                    jMenuBar1.add(jMenu7);

                    setJMenuBar(jMenuBar1);

                    pack();
                    setLocationRelativeTo(null);
          }// </editor-fold>//GEN-END:initComponents
        
        
        private void mnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnSalirActionPerformed
                  CerrarAplicacion();
        }//GEN-LAST:event_mnSalirActionPerformed

        private void mnGenerarNotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnGenerarNotaActionPerformed
                  //Actualiza cada vez que se selecciona ese panel
                  pnlGenerarNota.ActualizarDatos();

                //Visibilidad del Panel
                VisibilidadPaneles(false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false);
                contenedorAdmin.add(pnlGenerarNota);
                contenedorAdmin.validate();
        }//GEN-LAST:event_mnGenerarNotaActionPerformed

        private void mnReimprimirNotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnReimprimirNotaActionPerformed
                 //Visibilidad del Panel
                VisibilidadPaneles(false, false, true, false, false, false, false, false, false, false,false, false, false, false, false, false, false);
                contenedorAdmin.add(pnlReimprimirNota);
                contenedorAdmin.validate();
        }//GEN-LAST:event_mnReimprimirNotaActionPerformed

        private void mnAnularNotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnAnularNotaActionPerformed
                //Visibilidad del Panel
                VisibilidadPaneles(false, false, false, true, false, false, false, false, false, false,false, false, false, false, false, false, false);
                contenedorAdmin.add(pnlAnularNota);
                contenedorAdmin.validate();
        }//GEN-LAST:event_mnAnularNotaActionPerformed

        private void mnCambioTurnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnCambioTurnoActionPerformed
                //Visibilidad del Panel
                VisibilidadPaneles(false, false, false, false, true, false, false, false, false, false,false, false, false, false, false, false, false);
                contenedorAdmin.add(pnlTurno);
                contenedorAdmin.validate();
        }//GEN-LAST:event_mnCambioTurnoActionPerformed

        private void mnConductorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnConductorActionPerformed
                //Visibilidad del Panel
                VisibilidadPaneles(false, false, false, false, false, true, false, false, false, false,false, false, false, false, false, false, false);
                contenedorAdmin.add(pnlVentasConductor);
                contenedorAdmin.validate();
        }//GEN-LAST:event_mnConductorActionPerformed

        private void mnVehiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnVehiculoActionPerformed
                //Visibilidad del Panel
                VisibilidadPaneles(false, false, false, false, false, false, true, false, false, false,false, false, false, false, false, false, false);
                contenedorAdmin.add(pnlVentasVehiculo);
                contenedorAdmin.validate();
        }//GEN-LAST:event_mnVehiculoActionPerformed

        private void mnCaraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnCaraActionPerformed
                //Visibilidad del Panel
                VisibilidadPaneles(false, false, false, false, false, false, false, true, false, false,false, false, false, false, false, false, false);
                contenedorAdmin.add(pnlVentasCara);
                contenedorAdmin.validate();
        }//GEN-LAST:event_mnCaraActionPerformed

        private void mnMangueraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnMangueraActionPerformed
                //Visibilidad del Panel
                VisibilidadPaneles(false, false, false, false, false, false, false, false, true, false,false, false, false, false, false, false, false);
                contenedorAdmin.add(pnlVentasManguera);
                contenedorAdmin.validate();
        }//GEN-LAST:event_mnMangueraActionPerformed

        private void mnProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnProductoActionPerformed
                //Visibilidad del Panel
                VisibilidadPaneles(false, false, false, false, false, false, false, false, false, true,false, false, false, false, false, false, false);
                contenedorAdmin.add(pnlVentasProducto);
                contenedorAdmin.validate();
        }//GEN-LAST:event_mnProductoActionPerformed

        private void mnReporteTiempoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnReporteTiempoActionPerformed
                //Visibilidad del Panel
                VisibilidadPaneles(false, false, false, false, false, false, false, false, false, false,true, false, false, false, false, false, false);
                contenedorAdmin.add(pnlVentasTiempo);
                contenedorAdmin.validate();
        }//GEN-LAST:event_mnReporteTiempoActionPerformed

        private void mnCerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnCerrarSesionActionPerformed
                  CerrarSesion();

        }//GEN-LAST:event_mnCerrarSesionActionPerformed

        private void mnDataBaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnDataBaseActionPerformed
                //Visibilidad del Panel
                VisibilidadPaneles(false, false, false, false, false, false, false, false, false, false,false,false, true, false, false, false, false);
                contenedorAdmin.add(pnlConfiguracion);
                contenedorAdmin.validate();
        }//GEN-LAST:event_mnDataBaseActionPerformed
    private void mnDatosGeneralesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
                VisibilidadPaneles(false, false, false, false, false, false, false, false, false, false,false, true, false, false, false, false, false);
                contenedorAdmin.add(pnlDatosGenerales);
                contenedorAdmin.validate();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
                VisibilidadPaneles(false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false);
                pnlChoferConfiguracion.ActualizarChoferes("");
                contenedorAdmin.add(pnlChoferConfiguracion);
                contenedorAdmin.validate();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
                VisibilidadPaneles(false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false);
                pnlVehiculosConfiguracion.ActualizarVehiculos(" ");
                contenedorAdmin.add(pnlVehiculosConfiguracion);
                contenedorAdmin.validate();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        // TODO add your handling code here:
                VisibilidadPaneles(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false);
                pnlUsuariosConfiguracion.ActualizarUsuarios(" ");
                contenedorAdmin.add(pnlUsuariosConfiguracion);
                contenedorAdmin.validate();
    }//GEN-LAST:event_jMenuItem4ActionPerformed
    private void mnCambioPrecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnCambioPrecioActionPerformed
                   //Actualiza cada vez que se selecciona ese panel
                  pnlCambioprecio.ActualizarDatos();

                  //Visibilidad del Panel
                  VisibilidadPaneles(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true);
                  contenedorAdmin.add(pnlCambioprecio);
                  contenedorAdmin.validate();
    }//GEN-LAST:event_mnCambioPrecioActionPerformed
        /**
         * @param args the command line arguments
         */
        public static void main(String args[]) {
                /* Set the Nimbus look and feel */
                //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
                /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
                 */
                try {
                        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                                if ("Nimbus".equals(info.getName())) {
                                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                                        break;
                                }
                        }
                } catch (ClassNotFoundException ex) {
                        java.util.logging.Logger.getLogger(ModuloAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                        java.util.logging.Logger.getLogger(ModuloAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                        java.util.logging.Logger.getLogger(ModuloAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                        java.util.logging.Logger.getLogger(ModuloAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
                //</editor-fold>

                /* Create and display the form */
                java.awt.EventQueue.invokeLater(new Runnable() {
                        public void run() {
                                new ModuloAdministrador().setVisible(true);
                        }
                });
        }

          // Variables declaration - do not modify//GEN-BEGIN:variables
          private javax.swing.JPanel contenedorAdmin;
          private javax.swing.JMenu jMenu1;
          private javax.swing.JMenu jMenu2;
          private javax.swing.JMenu jMenu3;
          private javax.swing.JMenu jMenu4;
          private javax.swing.JMenu jMenu6;
          private javax.swing.JMenu jMenu7;
          private javax.swing.JMenuBar jMenuBar1;
          private javax.swing.JMenuItem jMenuItem2;
          private javax.swing.JMenuItem jMenuItem3;
          private javax.swing.JMenuItem jMenuItem4;
          private javax.swing.JMenuItem mnAnularNota;
          private javax.swing.JMenuItem mnCambioPrecio;
          private javax.swing.JMenuItem mnCambioTurno;
          private javax.swing.JMenuItem mnCara;
          private javax.swing.JMenuItem mnCerrarSesion;
          private javax.swing.JMenuItem mnConductor;
          private javax.swing.JMenuItem mnDataBase;
          private javax.swing.JMenuItem mnDatosGenerales;
          private javax.swing.JMenuItem mnGenerarNota;
          private javax.swing.JMenuItem mnManguera;
          private javax.swing.JMenuItem mnProducto;
          private javax.swing.JMenuItem mnReimprimirNota;
          private javax.swing.JMenuItem mnReporteTiempo;
          private javax.swing.JMenuItem mnSalir;
          private javax.swing.JMenuItem mnVehiculo;
          // End of variables declaration//GEN-END:variables
}
