package ModuloInterfaz;

import ConexionBD.BaseDatos;
import ConexionBD.Usuario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import org.apache.commons.codec.digest.DigestUtils;

public class FrmLogeo extends javax.swing.JFrame {

          BaseDatos bd = new BaseDatos();
          Connection conexion;
          Usuario user = new Usuario();
          int contadorInt = 0;
          String fecha = "";

          Timer contCon = new Timer(1000, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                              contadorInt += 1;
                              System.out.println("Se comenzó un evento del contador " + contadorInt);

                              try {
                                        ResultSet rs2 = bd.Consultar("SELECT \"cmValor\" FROM \"aComandos\" WHERE \"cmNumero\" = 1", conexion);
                                        rs2.next();
                                        if (Integer.parseInt(rs2.getString(1)) == 0) {
                                                  ResultSet rs3 = bd.Consultar("SELECT * FROM \"aTurnos\" WHERE \"tnID\" = (SELECT MAX(\"tnID\") from \"aTurnos\")", conexion);
                                                  rs3.next();
                                                  String fecha2 = rs3.getString("tnHora");
                                                  if (fecha.substring(0, 10).equals(fecha2.substring(0, 10))) {
                                                            numTurno = numTurno + 1;
                                                  } else {
                                                            numTurno = 1;
                                                  }
                                                  user.setUsTurno(numTurno);
                                                  bd.Actualizar("UPDATE \"aTurnos\" SET \"tnusID\" = " + user.getUsID() + ", \"tnNumero\" = " + numTurno
                                                          + " WHERE \"tnID\" = (SELECT MAX(\"tnID\") from \"aTurnos\")", conexion);
                                                  if (user.getUsTipo().equals("Administrador")) {
                                                            ModuloAdministrador Administracion = new ModuloAdministrador();
                                                            Administracion.setVisible(true);
                                                            noVisible();
                                                  } else if (user.getUsTipo().equals("Usuario")) {
                                                            ModuloIsla Isla = new ModuloIsla();
                                                            Isla.setVisible(true);
                                                            noVisible();
                                                  }
                                                  contCon.stop();
                                                  contadorInt = 0;
                                        }
                              } catch (Exception ex) {
                                        System.out.println("Error en el contador");
                                        ex.printStackTrace();
                              }

                              if (contadorInt == 5) {
                                        contCon.stop();
                                        contadorInt = 0;
                                        JOptionPane.showMessageDialog(null, "Esta desconectado el controlador, conéctelo e intente de nuevo", "ALERTA", 2);
                                        bd.Actualizar("UPDATE \"aComandos\" SET \"cmValor\" = 0 WHERE \"cmNumero\" = 1", conexion);
                                        lblEspera.setVisible(false);
                              }
                    }
          });

          int numTurno = 0;
          Timer inicial = new Timer(500, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                              contadorInt += 1;
                              if (contadorInt == 1) {
                                        inicial.stop();
                                        FrmConexion frmConexion = new FrmConexion();
                                        frmConexion.setVisible(true);
                              }
                    }
          });
          
          public FrmLogeo() {
                    initComponents();
                    this.setTitle("Login");
                    this.setIconImage(new ImageIcon(getClass().getResource("/image/Proyecto.png")).getImage());
                    conexion = bd.Conectar();
                    if (conexion == null) {
                              //---------------------------Cambiar a mensaje de error---------------------------
                              JOptionPane.showMessageDialog(this, "No se pudo conectar con la base de datos, verifique "
                                      + "la conexión y reinicie el programa.", "Advertencia", JOptionPane.ERROR_MESSAGE);
                              
                              inicial.start();
                              
                    }
                    lblEspera.setVisible(false);
          }

          public void noVisible() {
                    this.setVisible(false);
          }

          public boolean validacion() { //VALIDA EL USUARIO NO SIRVE EL RETORNO 
                    String textoSinEncriptar = "admin123";
                    String textoEncriptadoConSHA = DigestUtils.sha1Hex(textoSinEncriptar);
                    System.out.println("Texto Encriptado con SHA : " + textoEncriptadoConSHA);

                    String username = txtIUsuario.getText();
                    String password = txtContraseña.getText();
                    if (username.equals("") || password.equals("")) {
                              JOptionPane.showMessageDialog(this, "Ingrese sus datos");
                              return false;
                    } else {
                              String cifrarPassword = DigestUtils.sha1Hex(password);
                              System.out.println(cifrarPassword);
                              String sentencia = "SELECT * FROM \"aUsuario\" WHERE \"usNickname\" = '" + username + "' AND \"usPassword\" = '" + cifrarPassword + "'";
                              try {
                                        ResultSet rs = bd.Consultar(sentencia, conexion);
                                        while (rs.next()) {
                                                  user.setUsID(rs.getInt(1));
                                                  user.setUsNombre(rs.getString(2).trim());
                                                  user.setUsDNI(rs.getInt(3));
                                                  user.setUsTipo(rs.getString(4).trim());
                                                  user.setUsNickname(rs.getString(5).trim());
                                                  user.setUsPassword(rs.getString(6).trim());
                                        }
                                        System.out.println(user.getUsTipo());
                                        return true;
                              } catch (SQLException e) {
                                        System.out.println("Error al validar los datos");
                                        JOptionPane.showMessageDialog(this, "Usuario y/o contraseña incorrectas");
                                        lblEspera.setVisible(false);
                              }
                    }
                    return false;

          }

          public void cambioDeTurno() {
                    System.out.println("wasa");
                    if (user.getUsTipo().equals("Usuario")) {
                              int usID = -1;
                              String sql = "SELECT * FROM \"aTurnos\" WHERE \"tnID\" = (SELECT MAX(\"tnID\") from \"aTurnos\")";
                              ResultSet rs = bd.Consultar(sql, conexion);
                              try {
                                        while (rs.next()) {
                                                  usID = Integer.parseInt(rs.getString("tnusID"));
                                                  numTurno = Integer.parseInt(rs.getString("tnNumero"));
                                                  fecha = rs.getString("tnHora");
                                        }
                                        if (user.getUsID() != usID) {
                                                  ResultSet rs2 = bd.Consultar("SELECT count(*) FROM \"aVenta\" WHERE \"veEstado\" = 0", conexion);
                                                  rs2.next();
                                                  int cantidad = Integer.parseInt(rs2.getString(1));
                                                  if (cantidad > 0) {
                                                            JOptionPane.showMessageDialog(null, "Para cambiar de turno no debe haber despachos sin que se haya generado su respectiva nota.", "ADVERTENCIA", 2);
                                                            lblEspera.setVisible(false);
                                                  } else {
                                                            bd.Actualizar("UPDATE \"aComandos\" SET \"cmValor\" = 1 WHERE \"cmNumero\" = 1", conexion);
                                                            contCon.start();
                                                  }
                                        } else {
                                                  obtenerTurno();
                                                  ModuloIsla Isla = new ModuloIsla();
                                                  Isla.setVisible(true);
                                                  noVisible();
                                        }
                              } catch (Exception ex) {
                                        System.out.println("Error de Obtencion de Turno en Logeo");
                                        ex.printStackTrace();
                              }
                    } else if (user.getUsTipo().equals("Administrador")) {
                              obtenerTurno();
                              ModuloAdministrador Administracion = new ModuloAdministrador();
                              Administracion.setVisible(true);
                              noVisible();
                    } else {
                              JOptionPane.showMessageDialog(this, "Usuario y/o contraseña incorrectas");
                              lblEspera.setVisible(false);
                    }
          }

          public void obtenerTurno() {
                    String sql = "SELECT \"tnNumero\" FROM \"aTurnos\" WHERE \"tnID\" = (SELECT MAX(\"tnID\") from \"aTurnos\")";
                    ResultSet rs = bd.Consultar(sql, conexion);
                    try {
                              rs.next();
                              user.setUsTurno(Integer.parseInt(rs.getString(1)));
                    } catch (Exception ex) {
                              System.out.println("Error al obtener el turno");
                              ex.printStackTrace();
                    }
          }

          @SuppressWarnings("unchecked")
          // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
          private void initComponents() {
                    java.awt.GridBagConstraints gridBagConstraints;

                    jLabel1 = new javax.swing.JLabel();
                    jLabel2 = new javax.swing.JLabel();
                    txtIUsuario = new javax.swing.JTextField();
                    jLabel3 = new javax.swing.JLabel();
                    txtContraseña = new javax.swing.JPasswordField();
                    btnAceptar = new javax.swing.JButton();
                    btnCancelar = new javax.swing.JButton();
                    jLabel4 = new javax.swing.JLabel();
                    lblEspera = new javax.swing.JLabel();

                    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                    setBackground(new java.awt.Color(0, 204, 204));
                    setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                    setPreferredSize(new java.awt.Dimension(450, 600));
                    getContentPane().setLayout(new java.awt.GridBagLayout());

                    jLabel1.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
                    jLabel1.setText("INGRESAR");
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = 0;
                    gridBagConstraints.gridwidth = 2;
                    gridBagConstraints.insets = new java.awt.Insets(0, 0, 20, 0);
                    getContentPane().add(jLabel1, gridBagConstraints);

                    jLabel2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                    jLabel2.setText("USUARIO:");
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = 3;
                    gridBagConstraints.insets = new java.awt.Insets(20, 20, 20, 20);
                    getContentPane().add(jLabel2, gridBagConstraints);

                    txtIUsuario.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 1;
                    gridBagConstraints.gridy = 3;
                    gridBagConstraints.ipadx = 150;
                    gridBagConstraints.insets = new java.awt.Insets(20, 20, 20, 20);
                    getContentPane().add(txtIUsuario, gridBagConstraints);

                    jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                    jLabel3.setText("CONTRASEÑA:");
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = 5;
                    gridBagConstraints.insets = new java.awt.Insets(20, 20, 20, 20);
                    getContentPane().add(jLabel3, gridBagConstraints);

                    txtContraseña.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
                    txtContraseña.addKeyListener(new java.awt.event.KeyAdapter() {
                              public void keyPressed(java.awt.event.KeyEvent evt) {
                                        txtContraseñaKeyPressed(evt);
                              }
                    });
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 1;
                    gridBagConstraints.gridy = 5;
                    gridBagConstraints.ipadx = 150;
                    gridBagConstraints.insets = new java.awt.Insets(20, 20, 20, 20);
                    getContentPane().add(txtContraseña, gridBagConstraints);

                    btnAceptar.setBackground(new java.awt.Color(0, 204, 153));
                    btnAceptar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                    btnAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Botones/acep_norm.png"))); // NOI18N
                    btnAceptar.setBorder(null);
                    btnAceptar.setBorderPainted(false);
                    btnAceptar.setContentAreaFilled(false);
                    btnAceptar.setFocusPainted(false);
                    btnAceptar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/Botones/acep_press.png"))); // NOI18N
                    btnAceptar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Botones/acep_roll.png"))); // NOI18N
                    btnAceptar.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        btnAceptarActionPerformed(evt);
                              }
                    });
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = 6;
                    gridBagConstraints.gridwidth = 2;
                    gridBagConstraints.insets = new java.awt.Insets(20, 0, 0, 150);
                    getContentPane().add(btnAceptar, gridBagConstraints);

                    btnCancelar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
                    btnCancelar.setText("CANCELAR");
                    btnCancelar.addActionListener(new java.awt.event.ActionListener() {
                              public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        btnCancelarActionPerformed(evt);
                              }
                    });
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = 6;
                    gridBagConstraints.gridwidth = 2;
                    gridBagConstraints.insets = new java.awt.Insets(20, 150, 0, 0);
                    getContentPane().add(btnCancelar, gridBagConstraints);

                    jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/User_Circle.png"))); // NOI18N
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = 2;
                    gridBagConstraints.gridwidth = 2;
                    gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
                    getContentPane().add(jLabel4, gridBagConstraints);

                    lblEspera.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
                    lblEspera.setForeground(new java.awt.Color(255, 0, 0));
                    lblEspera.setText("Ingresando...");
                    gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = 7;
                    gridBagConstraints.gridwidth = 2;
                    gridBagConstraints.ipady = 20;
                    getContentPane().add(lblEspera, gridBagConstraints);

                    pack();
                    setLocationRelativeTo(null);
          }// </editor-fold>//GEN-END:initComponents

        private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
                  lblEspera.setVisible(true);
                  user.setUsTipo(" ");
                  validacion();
                  cambioDeTurno();
        }//GEN-LAST:event_btnAceptarActionPerformed

        private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
                  // TODO add your handling code here:
                  System.exit(0);
        }//GEN-LAST:event_btnCancelarActionPerformed

        private void txtContraseñaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtContraseñaKeyPressed
                  int code = evt.getKeyCode();
                  if (code == KeyEvent.VK_ENTER) {
                            lblEspera.setVisible(true);
                            user.setUsTipo("");
                            validacion();
                            cambioDeTurno();
                  }
        }//GEN-LAST:event_txtContraseñaKeyPressed

          /**
           * @param args the command line arguments
           */
          public static void main(String args[]) {
                    /* Set the Nimbus look and feel */
                    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
                    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
                     */
                    try {
                              for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                                        if ("Nimbus".equals(info.getName())) {
                                                  javax.swing.UIManager.setLookAndFeel(info.getClassName());
                                                  break;
                                        }
                              }
                    } catch (ClassNotFoundException ex) {
                              java.util.logging.Logger.getLogger(FrmLogeo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                    } catch (InstantiationException ex) {
                              java.util.logging.Logger.getLogger(FrmLogeo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                              java.util.logging.Logger.getLogger(FrmLogeo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                              java.util.logging.Logger.getLogger(FrmLogeo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                    }
                    //</editor-fold>
                    //</editor-fold>

                    /* Create and display the form */
                    java.awt.EventQueue.invokeLater(new Runnable() {
                              public void run() {
                                        new FrmLogeo().setVisible(true);
                              }
                    });
          }

          // Variables declaration - do not modify//GEN-BEGIN:variables
          private javax.swing.JButton btnAceptar;
          private javax.swing.JButton btnCancelar;
          private javax.swing.JLabel jLabel1;
          private javax.swing.JLabel jLabel2;
          private javax.swing.JLabel jLabel3;
          private javax.swing.JLabel jLabel4;
          private javax.swing.JLabel lblEspera;
          private javax.swing.JPasswordField txtContraseña;
          private javax.swing.JTextField txtIUsuario;
          // End of variables declaration//GEN-END:variables
}
