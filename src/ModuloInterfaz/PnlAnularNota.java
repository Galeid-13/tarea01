package ModuloInterfaz;

import ConexionBD.BaseDatos;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Properties;
import javax.swing.JOptionPane;

public class PnlAnularNota extends javax.swing.JPanel {

          BaseDatos bd = new BaseDatos();
          Connection conexion;

          public PnlAnularNota() {
                    initComponents();

                    conexion = bd.Conectar();
                    if (conexion == null) {
                              //---------------------------Cambiar a mensaje de error---------------------------
                              int valor = JOptionPane.showConfirmDialog(this, "No se pudo conectar con la base de datos, verifique "
                                      + "la conexión y reinicie el programa.", "Advertencia", JOptionPane.ERROR_MESSAGE);
                    }
          }

          public String loadConfig(String value) {
                    Properties config = new Properties();
                    InputStream configInput = null;
                    OutputStream configOutput = null;
                    try {
                              configInput = new FileInputStream("src/data/nameProducto.properties");
                              String valor;
                              //configInput = ClassLoader.getSystemResourceAsStream("data/nameProducto.properties");
                              config.load(configInput);
                              valor = config.getProperty(value);
                              configInput.close();
                              return valor;
                    } catch (Exception e) {
                              JOptionPane.showMessageDialog(null, "Error cargando configuración\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    return null;
          }

          public String obtenerDNIChofer(String idChofer) {
                    if (idChofer == null) {
                              idChofer = "0";
                    }
                    String sql = "SELECT \"chDNI\" FROM \"aChoferes\" WHERE \"chID\" = " + idChofer;
                    ResultSet rs = bd.Consultar(sql, conexion);
                    try {
                              rs.next();
                              if (rs.getString(1) != null) {
                                        return rs.getString(1);
                              }
                    } catch (Exception ex) {
                              System.out.println("Error al obtener dni chofer");
                              ex.printStackTrace();
                    }
                    return "00000000";
          }

          public String obtenerPlaca(String idVehiculo) {
                    if (idVehiculo == null) {
                              idVehiculo = "0";
                    }
                    String sql = "SELECT \"vhPlaca\" FROM \"aVehiculos\" WHERE \"vhID\" = " + idVehiculo;
                    ResultSet rs = bd.Consultar(sql, conexion);
                    try {
                              rs.next();
                              if (rs.getString(1) != null) {
                                        return rs.getString(1);
                              }
                    } catch (Exception ex) {
                              System.out.println("Error al obtener dni chofer");
                              ex.printStackTrace();
                    }
                    return "AAA000";
          }

          public void Operacion() {
                    String sql = "SELECT * FROM \"aNotas\" WHERE \"ntNroSerie\" = '" + txtSerie.getText() + "' AND  \"ntNroNota\" = '" + txtNumero.getText() + "'";
                    ResultSet rs = bd.Consultar(sql, null);
                    String dni = "", placa = "", odometro = "", descripcion = "", pUnitario = "", cantidad = "", importe = "", idVenta = "", estado = "";
                    String unidades = "GLS";
                    String accion = "ANULAR";
                    try {
                              while (rs.next()) {
                                        dni = obtenerDNIChofer(rs.getString("ntIDChofer"));
                                        placa = obtenerPlaca(rs.getString("ntIDVehiculo"));
                                        odometro = rs.getString("ntOdometro");
                                        idVenta = rs.getString("ntIDVenta");
                                        estado = rs.getString("ntEstado");
                                        if (estado.equals("A")) {
                                                  JOptionPane.showMessageDialog(this, "Ya se anulo este comprobante");
                                        } else {
                                                  System.out.println(estado + "aqui el estado de accion");
                                                  String sql2 = "SELECT * FROM \"aVenta\" WHERE \"veID\" = " + idVenta;
                                                  ResultSet rs2 = bd.Consultar(sql2, null);
                                                  while (rs2.next()) {
                                                            descripcion = loadConfig(rs2.getString("veProducto"));
                                                            pUnitario = rs2.getString("vePrecio");
                                                            cantidad = rs2.getString("veVolumen");
                                                            importe = rs2.getString("veImporte");
                                                  }
                                                  //Enviar datos al JFrame FrmDatosDespacho 
                                                  FrmDatosDespacho frmDatosDespacho = new FrmDatosDespacho(dni, placa, odometro, descripcion, pUnitario,
                                                          cantidad, importe, unidades, txtNumero.getText(), txtSerie.getText(), accion, idVenta);
                                                  frmDatosDespacho.setVisible(true);
                                        }
                              }

                    } catch (Exception ex) {
                              System.out.println("Error al Mostrar los datos de la reimpresión");
                              ex.printStackTrace();
                    }
          }

          public void Numeros(KeyEvent evt) {
                    char caracter = evt.getKeyChar();
                    if (Character.isLetter(caracter)) {
                              getToolkit().beep();
                              evt.consume();
                              //JOptionPane.showMessageDialog(rootPane, "Ingresar solo Numeros");
                    }
          }

          @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtSerie = new javax.swing.JTextField();
        txtNumero = new javax.swing.JTextField();
        btnAceptar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        jLabel1.setText("ANULAR NOTA DE DESPACHO");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        add(jLabel1, gridBagConstraints);

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel2.setText("NOTA DE DESPACHO SERIE");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(30, 0, 0, 0);
        add(jLabel2, gridBagConstraints);

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel3.setText("NOTA DE DESPACHO NUMERO");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 0, 0);
        add(jLabel3, gridBagConstraints);

        txtSerie.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtSerie.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSerieKeyTyped(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 150;
        gridBagConstraints.insets = new java.awt.Insets(30, 0, 0, 0);
        add(txtSerie, gridBagConstraints);

        txtNumero.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtNumero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNumeroKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNumeroKeyTyped(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 150;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 0, 0);
        add(txtNumero, gridBagConstraints);

        btnAceptar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnAceptar.setText("ACEPTAR");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(30, 0, 0, 170);
        add(btnAceptar, gridBagConstraints);

        btnSalir.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnSalir.setText("SALIR");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(30, 170, 0, 0);
        add(btnSalir, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

        private void txtNumeroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumeroKeyPressed
                  int code = evt.getKeyCode();
                  if (code == KeyEvent.VK_ENTER) {
                            Operacion();
                  }
        }//GEN-LAST:event_txtNumeroKeyPressed

        private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
                  Operacion();
        }//GEN-LAST:event_btnAceptarActionPerformed

        private void txtNumeroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumeroKeyTyped
                  Numeros(evt);
        }//GEN-LAST:event_txtNumeroKeyTyped

        private void txtSerieKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSerieKeyTyped
                  Numeros(evt);
        }//GEN-LAST:event_txtSerieKeyTyped

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
              // TODO add your handling code here:
              int valor = JOptionPane.showConfirmDialog(this, "¿Está seguro de cerrar la aplicación?", "Advertencia", JOptionPane.YES_NO_OPTION);
              if (valor == JOptionPane.YES_OPTION) {
                        System.exit(0);
              }
    }//GEN-LAST:event_btnSalirActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField txtNumero;
    private javax.swing.JTextField txtSerie;
    // End of variables declaration//GEN-END:variables
}
