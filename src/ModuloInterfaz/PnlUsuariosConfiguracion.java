/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloInterfaz;

import ConexionBD.BaseDatos;
import ConexionBD.Usuario;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.codec.digest.DigestUtils;


public class PnlUsuariosConfiguracion extends javax.swing.JPanel {

    BaseDatos bd = new BaseDatos();
    Connection conexion = bd.Conectar();
    ArrayList<Integer> idUsuarios;
    String dniUsuario = "";
    
    public PnlUsuariosConfiguracion() {
        initComponents();
        idUsuarios = new ArrayList<>();
    }
    
    public void ActualizarUsuarios(String condicion) {
        try {
            idUsuarios.clear();
            DefaultTableModel modelo = new DefaultTableModel();
            tblUsuarios.setModel(modelo);
            String sql = "SELECT * FROM \"aUsuario\" " + condicion;
            ResultSet rs = bd.Consultar(sql, conexion);
            Object[] nombresColumnas = new Object[]{"NOMBRE", "DNI", "TIPO", "USUARIO"};
            modelo.setColumnIdentifiers(nombresColumnas);
            while (rs.next()) {
                idUsuarios.add(Integer.parseInt(rs.getString("usID")));
                Object[] fila = new Object[]{
                    rs.getString("usNombre").trim(),
                    rs.getString("usDNI"),
                    rs.getString("usTipo").trim(),
                    rs.getString("usNickname").trim(),
                };
                modelo.addRow(fila);
                tblUsuarios.setModel(modelo);
            }
            return;
        } catch (Exception ex) {
            System.out.println("Error al actualizar los usuarios");
            ex.printStackTrace();
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jScrollPane1 = new javax.swing.JScrollPane();
        tblUsuarios = new javax.swing.JTable();
        txtDNI = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        btnNuevo = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        btnClave = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        jScrollPane1.setPreferredSize(new java.awt.Dimension(500, 300));

        tblUsuarios.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        tblUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "NOMBRE", "DNI", "TIPO", "USUARIO"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblUsuarios.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tblUsuarios.setMinimumSize(new java.awt.Dimension(45, 208));
        tblUsuarios.setPreferredSize(null);
        jScrollPane1.setViewportView(tblUsuarios);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.gridheight = 6;
        add(jScrollPane1, gridBagConstraints);

        txtDNI.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtDNI.setPreferredSize(new java.awt.Dimension(300, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 15, 0);
        add(txtDNI, gridBagConstraints);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel1.setText("Buscar por DNI:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        add(jLabel1, gridBagConstraints);

        btnNuevo.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.setPreferredSize(new java.awt.Dimension(133, 27));
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new java.awt.Insets(25, 15, 25, 0);
        add(btnNuevo, gridBagConstraints);

        btnEditar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.setPreferredSize(new java.awt.Dimension(133, 27));
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.insets = new java.awt.Insets(15, 15, 25, 0);
        add(btnEditar, gridBagConstraints);

        btnBuscar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.setPreferredSize(new java.awt.Dimension(133, 27));
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 0, 0);
        add(btnBuscar, gridBagConstraints);

        btnClave.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnClave.setText("Cambiar Clave");
        btnClave.setToolTipText("");
        btnClave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClaveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.insets = new java.awt.Insets(15, 15, 25, 0);
        add(btnClave, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        FrmDatosFormulario datosUsuario = new FrmDatosFormulario("Nombre:", "DNI:", "Tipo:", "Nombre de Usuario:", "Contraseña:", "Agregar", "Usuarios", "Nuevo Usuario");
        datosUsuario.setVisible(true);
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        int filaSeleccionada = tblUsuarios.getSelectedRow();
        if (filaSeleccionada < 0) {
            JOptionPane.showMessageDialog(this, "Seleccione un Usuario");
        } else {
            int idUsu = idUsuarios.get(filaSeleccionada);
            String nombre = tblUsuarios.getValueAt(filaSeleccionada, 0).toString();
            String dni = tblUsuarios.getValueAt(filaSeleccionada, 1).toString();
            String tipo = tblUsuarios.getValueAt(filaSeleccionada, 2).toString();
            String usuario = tblUsuarios.getValueAt(filaSeleccionada, 3).toString();
            FrmDatosFormulario datosUsu = new FrmDatosFormulario("Nombre:", "DNI:", "Tipo: ", "Nombre de Usuario:", "0", "Editar", "Usuarios", "Editar Usuario");
            datosUsu.AsignarDatos(nombre, dni, tipo, usuario, null);
            datosUsu.setID(idUsu);
            datosUsu.setVisible(true);
        }
        System.out.println(filaSeleccionada);
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        String txtDNIString = txtDNI.getText();
        if(txtDNIString.equals("")){
            ActualizarUsuarios("");
        }else{
            ActualizarUsuarios("WHERE CAST(\"usDNI\" as varchar(8)) LIKE '%" + txtDNIString + "%'");
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnClaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClaveActionPerformed
        // TODO add your handling code here:
        int filaSeleccionada = tblUsuarios.getSelectedRow();
        if (filaSeleccionada < 0) {
            JOptionPane.showMessageDialog(this, "Seleccione un Usuario");
        } else {
            int idUsu = idUsuarios.get(filaSeleccionada);
            String nuevaClave = JOptionPane.showInputDialog(this, "Ingresar nueva clave del usuario", "Observación", 3);
            if(nuevaClave.equals("")){
                JOptionPane.showMessageDialog(null, "Ingrese Nueva Clave");
            }else{
                Usuario user = new Usuario();
                nuevaClave = DigestUtils.sha1Hex(nuevaClave);
                String confirmar = JOptionPane.showInputDialog(this, "Escribe tu contraseña para confirmar", "Observación", 3);
                confirmar = DigestUtils.sha1Hex(confirmar);
                if(confirmar.equals(user.getUsPassword())){
                    bd.Actualizar("UPDATE \"aUsuario\" SET \"usPassword\" = '" + nuevaClave + "' WHERE \"usID\" = " + idUsu, conexion);
                    JOptionPane.showMessageDialog(this, "Clave actualizada correctamente");
                }else{
                    JOptionPane.showMessageDialog(this, "Contraseña incorrecta, intente de nuevo");
                }
            }
        }
    }//GEN-LAST:event_btnClaveActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnClave;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblUsuarios;
    private javax.swing.JTextField txtDNI;
    // End of variables declaration//GEN-END:variables
}
