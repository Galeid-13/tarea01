package DataSource;

public class Producto {
        String item;
        String cantidadDespachos;
        String producto;
        String unidades;
        String volumen;
        String importe;

        public Producto() {
        }

        public Producto(String item, String cantidadDespachos, String producto, String unidades, String volumen, String importe) {
                this.item = item;
                this.cantidadDespachos = cantidadDespachos;
                this.producto = producto;
                this.unidades = unidades;
                this.volumen = volumen;
                this.importe = importe;
        }

        public String getItem() {
                return item;
        }

        public void setItem(String item) {
                this.item = item;
        }

        public String getCantidadDespachos() {
                return cantidadDespachos;
        }

        public void setCantidadDespachos(String cantidadDespachos) {
                this.cantidadDespachos = cantidadDespachos;
        }

        public String getProducto() {
                return producto;
        }

        public void setProducto(String producto) {
                this.producto = producto;
        }

        public String getUnidades() {
                return unidades;
        }

        public void setUnidades(String unidades) {
                this.unidades = unidades;
        }

        public String getVolumen() {
                return volumen;
        }

        public void setVolumen(String volumen) {
                this.volumen = volumen;
        }

        public String getImporte() {
                return importe;
        }

        public void setImporte(String importe) {
                this.importe = importe;
        }

        
        
        
}
