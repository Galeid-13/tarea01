package DataSource;


public class Manguera {
        String item;
        String cantidadDespachos;
        String cara;
        String manguera;
        String producto;
        String unidades;
        String precio;
        String volumen;
        String importe;

        public Manguera() {
        }

        public Manguera(String item, String cantidadDespachos, String cara, String manguera, String producto, String unidades, String precio, String volumen, String importe) {
                this.item = item;
                this.cantidadDespachos = cantidadDespachos;
                this.cara = cara;
                this.manguera = manguera;
                this.producto = producto;
                this.unidades = unidades;
                this.precio = precio;
                this.volumen = volumen;
                this.importe = importe;
        }

        public String getItem() {
                return item;
        }

        public void setItem(String item) {
                this.item = item;
        }

        public String getCantidadDespachos() {
                return cantidadDespachos;
        }

        public void setCantidadDespachos(String cantidadDespachos) {
                this.cantidadDespachos = cantidadDespachos;
        }

        public String getCara() {
                return cara;
        }

        public void setCara(String cara) {
                this.cara = cara;
        }

        public String getManguera() {
                return manguera;
        }

        public void setManguera(String manguera) {
                this.manguera = manguera;
        }

        public String getProducto() {
                return producto;
        }

        public void setProducto(String producto) {
                this.producto = producto;
        }

        public String getUnidades() {
                return unidades;
        }

        public void setUnidades(String unidades) {
                this.unidades = unidades;
        }

        public String getPrecio() {
                return precio;
        }

        public void setPrecio(String precio) {
                this.precio = precio;
        }

        public String getVolumen() {
                return volumen;
        }

        public void setVolumen(String volumen) {
                this.volumen = volumen;
        }

        public String getImporte() {
                return importe;
        }

        public void setImporte(String importe) {
                this.importe = importe;
        }

        
        
        
}
