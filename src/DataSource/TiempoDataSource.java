package DataSource;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;


public class TiempoDataSource implements JRDataSource{
        
        private List<Tiempo> listaTiempo = new ArrayList<Tiempo>();
        private int indiceParticipante = -1;

        public TiempoDataSource() {
        }

        @Override
        public boolean next() throws JRException {
                return ++indiceParticipante < listaTiempo.size();
        }

        public void addTiempo(Tiempo item){
                this.listaTiempo.add(item);
        }
        
        @Override
        public Object getFieldValue(JRField jrf) throws JRException {
                Object valor = null;
                
                if ("item".equals(jrf.getName())) {
                        valor = listaTiempo.get(indiceParticipante).getItem();
                }
                else if ("fechaHora".equals(jrf.getName())) {
                        valor = listaTiempo.get(indiceParticipante).getFechaHora();
                }
                else if ("cara".equals(jrf.getName())) {
                        valor = listaTiempo.get(indiceParticipante).getCara();
                }
                else if ("producto".equals(jrf.getName())) {
                        valor = listaTiempo.get(indiceParticipante).getProducto();
                }
                else if ("unidades".equals(jrf.getName())) {
                        valor = listaTiempo.get(indiceParticipante).getUnidades();
                }
                else if ("precio".equals(jrf.getName())) {
                        valor = listaTiempo.get(indiceParticipante).getPrecio();
                }
                else if ("volumen".equals(jrf.getName())) {
                        valor = listaTiempo.get(indiceParticipante).getVolumen();
                }
                else if ("importe".equals(jrf.getName())) {
                        valor = listaTiempo.get(indiceParticipante).getImporte();
                }
                return valor;
        }
        
}
