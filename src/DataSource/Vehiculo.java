package DataSource;

public class Vehiculo {
        String item;
        String placaVehiculo;
        String marca;
        String producto;
        String unidades;
        String volumen;
        String importe;

        public Vehiculo() {
        }

        public Vehiculo(String item, String placaVehiculo, String marca, String producto, String unidades, String volumen, String importe) {
                this.item = item;
                this.placaVehiculo = placaVehiculo;
                this.marca = marca;
                this.producto = producto;
                this.unidades = unidades;
                this.volumen = volumen;
                this.importe = importe;
        }

        public String getItem() {
                return item;
        }

        public void setItem(String item) {
                this.item = item;
        }

        public String getPlacaVehiculo() {
                return placaVehiculo;
        }

        public void setPlacaVehiculo(String placaVehiculo) {
                this.placaVehiculo = placaVehiculo;
        }

        public String getMarca() {
                return marca;
        }

        public void setMarca(String marca) {
                this.marca = marca;
        }

        public String getProducto() {
                return producto;
        }

        public void setProducto(String producto) {
                this.producto = producto;
        }

        public String getUnidades() {
                return unidades;
        }

        public void setUnidades(String unidades) {
                this.unidades = unidades;
        }

        public String getVolumen() {
                return volumen;
        }

        public void setVolumen(String volumen) {
                this.volumen = volumen;
        }

        public String getImporte() {
                return importe;
        }

        public void setImporte(String importe) {
                this.importe = importe;
        }
        
        
}
