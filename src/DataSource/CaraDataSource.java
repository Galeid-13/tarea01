package DataSource;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;


public class CaraDataSource implements JRDataSource{
        
        private List<Cara> listaCara = new ArrayList<Cara>();
        private int indiceParticipante = -1;

        public CaraDataSource() {
        }

        @Override
        public boolean next() throws JRException {
                return ++indiceParticipante < listaCara.size();
        }

        public void addCara(Cara item){
                this.listaCara.add(item);
        }
        
        @Override
        public Object getFieldValue(JRField jrf) throws JRException {
                Object valor = null;
                
                if ("item".equals(jrf.getName())) {
                        valor = listaCara.get(indiceParticipante).getItem();
                }
                else if ("cantidadDespachos".equals(jrf.getName())) {
                        valor = listaCara.get(indiceParticipante).getCantidadDespachos();    
                }
                else if ("cara".equals(jrf.getName())) {
                        valor = listaCara.get(indiceParticipante).getCara();
                }
                else if ("unidades".equals(jrf.getName())) {
                        valor = listaCara.get(indiceParticipante).getUnidades();
                }
                else if ("volumen".equals(jrf.getName())) {
                        valor = listaCara.get(indiceParticipante).getVolumen();
                }
                else if ("importe".equals(jrf.getName())) {
                        valor = listaCara.get(indiceParticipante).getImporte();
                }
                return valor;
        }
        
}
