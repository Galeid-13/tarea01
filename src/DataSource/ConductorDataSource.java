package DataSource;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;


public class ConductorDataSource implements JRDataSource{
        
        private List<Conductor> listaConductor = new ArrayList<Conductor>();
        private int indiceParticipante = -1;

        public ConductorDataSource() {
        }

        @Override
        public boolean next() throws JRException {
                return ++indiceParticipante < listaConductor.size();
        }

        public void addConductor(Conductor item){
                this.listaConductor.add(item);
        }
        
        @Override
        public Object getFieldValue(JRField jrf) throws JRException {
                Object valor = null;
                
                if ("item".equals(jrf.getName())) {
                        valor = listaConductor.get(indiceParticipante).getItem();
                }
                else if ("nombreConductor".equals(jrf.getName())) {
                        valor = listaConductor.get(indiceParticipante).getNombreConductor();
                        
                }
                else if ("DNI".equals(jrf.getName())) {
                        valor = listaConductor.get(indiceParticipante).getDNI();
                }
                else if ("producto".equals(jrf.getName())) {
                        valor = listaConductor.get(indiceParticipante).getProducto();
                }
                else if ("unidades".equals(jrf.getName())) {
                        valor = listaConductor.get(indiceParticipante).getUnidades();
                }
                else if ("volumen".equals(jrf.getName())) {
                        valor = listaConductor.get(indiceParticipante).getVolumen();
                }
                else if ("importe".equals(jrf.getName())) {
                        valor = listaConductor.get(indiceParticipante).getImporte();
                }
                return valor;
        }
        
}
