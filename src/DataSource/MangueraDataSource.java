package DataSource;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;


public class MangueraDataSource implements JRDataSource{
        
        private List<Manguera> listaManguera = new ArrayList<Manguera>();
        private int indiceParticipante = -1;

        public MangueraDataSource() {
        }

        @Override
        public boolean next() throws JRException {
                return ++indiceParticipante < listaManguera.size();
        }

        public void addManguera(Manguera item){
                this.listaManguera.add(item);
        }
        
        @Override
        public Object getFieldValue(JRField jrf) throws JRException {
                Object valor = null;
                
                if ("item".equals(jrf.getName())) {
                        valor = listaManguera.get(indiceParticipante).getItem();
                }
                else if ("cantidadDespachos".equals(jrf.getName())) {
                        valor = listaManguera.get(indiceParticipante).getCantidadDespachos();
                }
                else if ("cara".equals(jrf.getName())) {
                        valor = listaManguera.get(indiceParticipante).getCara();
                }
                else if ("manguera".equals(jrf.getName())) {
                        valor = listaManguera.get(indiceParticipante).getManguera();
                }
                else if ("producto".equals(jrf.getName())) {
                        valor = listaManguera.get(indiceParticipante).getProducto();
                }
                else if ("unidades".equals(jrf.getName())) {
                        valor = listaManguera.get(indiceParticipante).getUnidades();
                }
                else if ("precio".equals(jrf.getName())) {
                        valor = listaManguera.get(indiceParticipante).getPrecio();
                }
                else if ("volumen".equals(jrf.getName())) {
                        valor = listaManguera.get(indiceParticipante).getVolumen();
                }
                else if ("importe".equals(jrf.getName())) {
                        valor = listaManguera.get(indiceParticipante).getImporte();
                }
                return valor;
        }
        
}
