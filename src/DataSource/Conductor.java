package DataSource;

public class Conductor {
        String item;
        String nombreConductor;
        String DNI;
        String producto;
        String unidades;
        String volumen;
        String importe;
        
        public Conductor() {
        }

        public Conductor(String item, String nombreConductor, String DNI, String producto, String unidades, String volumen, String importe) {
                this.item = item;
                this.nombreConductor = nombreConductor;
                this.DNI = DNI;
                this.producto = producto;
                this.unidades = unidades;
                this.volumen = volumen;
                this.importe = importe;
        }

        public String getItem() {
                return item;
        }

        public void setItem(String item) {
                this.item = item;
        }

        public String getNombreConductor() {
                return nombreConductor;
        }

        public void setNombreConductor(String nombreConductor) {
                this.nombreConductor = nombreConductor;
        }

        public String getDNI() {
                return DNI;
        }

        public void setDNI(String DNI) {
                this.DNI = DNI;
        }

        public String getProducto() {
                return producto;
        }

        public void setProducto(String producto) {
                this.producto = producto;
        }

        public String getUnidades() {
                return unidades;
        }

        public void setUnidades(String unidades) {
                this.unidades = unidades;
        }

        public String getVolumen() {
                return volumen;
        }

        public void setVolumen(String volumen) {
                this.volumen = volumen;
        }

        public String getImporte() {
                return importe;
        }

        public void setImporte(String importe) {
                this.importe = importe;
        }


}
