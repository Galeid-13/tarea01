package DataSource;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;


public class VehiculoDataSource implements JRDataSource{
        
        private List<Vehiculo> listaVehiculo = new ArrayList<Vehiculo>();
        private int indiceParticipante = -1;

        public VehiculoDataSource() {
        }

        @Override
        public boolean next() throws JRException {
                return ++indiceParticipante < listaVehiculo.size();
        }

        public void addVehiculo(Vehiculo item){
                this.listaVehiculo.add(item);
        }
        
        @Override
        public Object getFieldValue(JRField jrf) throws JRException {
                Object valor = null;
                
                if ("item".equals(jrf.getName())) {
                        valor = listaVehiculo.get(indiceParticipante).getItem();
                }
                else if ("placaVehiculo".equals(jrf.getName())) {
                        valor = listaVehiculo.get(indiceParticipante).getPlacaVehiculo();
                        
                }
                else if ("marca".equals(jrf.getName())) {
                        valor = listaVehiculo.get(indiceParticipante).getMarca();
                }
                else if ("producto".equals(jrf.getName())) {
                        valor = listaVehiculo.get(indiceParticipante).getProducto();
                }
                else if ("unidades".equals(jrf.getName())) {
                        valor = listaVehiculo.get(indiceParticipante).getUnidades();
                }
                else if ("volumen".equals(jrf.getName())) {
                        valor = listaVehiculo.get(indiceParticipante).getVolumen();
                }
                else if ("importe".equals(jrf.getName())) {
                        valor = listaVehiculo.get(indiceParticipante).getImporte();
                }
                return valor;
        }
        
}
