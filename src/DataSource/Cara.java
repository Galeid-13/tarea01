package DataSource;


public class Cara {
        String item;
        String cantidadDespachos;
        String cara;
        String unidades;
        String volumen;
        String importe;

        public Cara() {
        }

        public Cara(String item, String cantidadDespachos, String cara, String unidades, String volumen, String importe) {
                this.item = item;
                this.cantidadDespachos = cantidadDespachos;
                this.cara = cara;
                this.unidades = unidades;
                this.volumen = volumen;
                this.importe = importe;
        }

        public String getItem() {
                return item;
        }

        public void setItem(String item) {
                this.item = item;
        }

        public String getCantidadDespachos() {
                return cantidadDespachos;
        }

        public void setCantidadDespachos(String cantidadDespachos) {
                this.cantidadDespachos = cantidadDespachos;
        }

        public String getCara() {
                return cara;
        }

        public void setCara(String cara) {
                this.cara = cara;
        }

        public String getUnidades() {
                return unidades;
        }

        public void setUnidades(String unidades) {
                this.unidades = unidades;
        }

        public String getVolumen() {
                return volumen;
        }

        public void setVolumen(String volumen) {
                this.volumen = volumen;
        }

        public String getImporte() {
                return importe;
        }

        public void setImporte(String importe) {
                this.importe = importe;
        }
        
        
}
