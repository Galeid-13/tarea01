package DataSource;

public class Tiempo {
        String item;
        String fechaHora;
        String cara;
        String producto;
        String unidades;
        String precio;
        String volumen;
        String importe;
        //String conductor;
        //String placa;

        public Tiempo() {
        }

        public Tiempo(String item, String fechaHora, String cara, String producto, String unidades, String precio, String volumen, String importe) {
                this.item = item;
                this.fechaHora = fechaHora;
                this.cara = cara;
                this.producto = producto;
                this.unidades = unidades;
                this.precio = precio;
                this.volumen = volumen;
                this.importe = importe;
        }

        public String getItem() {
                return item;
        }

        public void setItem(String item) {
                this.item = item;
        }

        public String getFechaHora() {
                return fechaHora;
        }

        public void setFechaHora(String fechaHora) {
                this.fechaHora = fechaHora;
        }

        public String getCara() {
                return cara;
        }

        public void setCara(String cara) {
                this.cara = cara;
        }

        public String getProducto() {
                return producto;
        }

        public void setProducto(String producto) {
                this.producto = producto;
        }

        public String getUnidades() {
                return unidades;
        }

        public void setUnidades(String unidades) {
                this.unidades = unidades;
        }

        public String getPrecio() {
                return precio;
        }

        public void setPrecio(String precio) {
                this.precio = precio;
        }

        public String getVolumen() {
                return volumen;
        }

        public void setVolumen(String volumen) {
                this.volumen = volumen;
        }

        public String getImporte() {
                return importe;
        }

        public void setImporte(String importe) {
                this.importe = importe;
        }


        
        
        
}
