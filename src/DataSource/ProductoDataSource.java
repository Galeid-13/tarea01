package DataSource;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;


public class ProductoDataSource implements JRDataSource{
        
        private List<Producto> listaProducto = new ArrayList<Producto>();
        private int indiceParticipante = -1;

        public ProductoDataSource() {
        }

        @Override
        public boolean next() throws JRException {
                return ++indiceParticipante < listaProducto.size();
        }

        public void addProducto(Producto item){
                this.listaProducto.add(item);
        }
        
        @Override
        public Object getFieldValue(JRField jrf) throws JRException {
                Object valor = null;
                
                if ("item".equals(jrf.getName())) {
                        valor = listaProducto.get(indiceParticipante).getItem();
                }
                else if ("cantidadDespachos".equals(jrf.getName())) {
                        valor = listaProducto.get(indiceParticipante).getCantidadDespachos();
                }
                else if ("producto".equals(jrf.getName())) {
                        valor = listaProducto.get(indiceParticipante).getProducto();
                }
                else if ("unidades".equals(jrf.getName())) {
                        valor = listaProducto.get(indiceParticipante).getUnidades();
                }
                else if ("volumen".equals(jrf.getName())) {
                        valor = listaProducto.get(indiceParticipante).getVolumen();
                }
                else if ("importe".equals(jrf.getName())) {
                        valor = listaProducto.get(indiceParticipante).getImporte();
                }
                return valor;
        }
        
}
