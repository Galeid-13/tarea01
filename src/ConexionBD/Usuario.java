
package ConexionBD;

public class Usuario {
        public static int usID;
        public static String usNombre;
        public static int usDNI;
        public static String usTipo;
        public static String usNickname;
        public static String usPassword;
        public static int usTurno;

        public static int getUsTurno() {
            return usTurno;
        }

        public static void setUsTurno(int usTurno) {
            Usuario.usTurno = usTurno;
        }

        public Usuario() {
        } 
        public static int getUsID() {
                return usID;
        }

        public static void setUsID(int aUsID) {
                usID = aUsID;
        }

        public static String getUsNombre() {
                return usNombre;
        }

        public static void setUsNombre(String aUsNombre) {
                usNombre = aUsNombre;
        }

        public static int getUsDNI() {
                return usDNI;
        }

        public static void setUsDNI(int aUsDNI) {
                usDNI = aUsDNI;
        }

        public static String getUsTipo() {
                return usTipo;
        }

        public static void setUsTipo(String aUsTipo) {
                usTipo = aUsTipo;
        }

        public static String getUsNickname() {
                return usNickname;
        }

        public static void setUsNickname(String aUsNickname) {
                usNickname = aUsNickname;
        }

        public static String getUsPassword() {
                return usPassword;
        }

        public static void setUsPassword(String aUsPassword) {
                usPassword = aUsPassword;
        }


  
}
