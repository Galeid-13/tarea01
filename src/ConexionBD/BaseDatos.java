package ConexionBD;

import java.awt.Component;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import javax.swing.JOptionPane;

public class BaseDatos {

        
        String direccion = "", puerto = "", baseDatos = "", usuario = "", clave = "", driver = "", url = "";
        Properties config = new Properties();
        InputStream configInput = null;

        public void loadConfig() {
                try {
                        //configInput = new FileInputStream("src/data/config.properties");
                        //configInput = ClassLoader.getSystemResourceAsStream("data/config.properties");
                        configInput = new FileInputStream("src/data/config.properties");
                        config.load(configInput);
                        direccion = config.getProperty("direccion");
                        puerto = config.getProperty("puerto");
                        baseDatos = config.getProperty("baseDatos");
                        usuario = config.getProperty("usuario");
                        clave = config.getProperty("clave");

                        //Variables de la Conexion
                        driver = "org.postgresql.Driver";
                        url = "jdbc:postgresql://" + direccion + ":" + puerto + "/" + baseDatos;
                } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Error cargando configuración\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
        }
        //Funcion que permite conectar con la base de datos en PostgreSQ
        
        public Connection Conectar() { //Lo cambie de private a public 
                loadConfig();
                System.out.println(url);
                try {
                        Class.forName(driver);
                        Connection conexion = DriverManager.getConnection(url, usuario, clave);
                        System.out.println("Conexion a la Base de Datos");
                        return conexion;
                } catch (Exception e) {
                        System.out.println("Problemas de Conexion");
                }
                return null;
        }

        public void Actualizar(String actualizacion, Connection conexion) {
                Statement st;
                try {
                        if (conexion == null) {
                                conexion = this.Conectar();
                        }
                        st = conexion.createStatement();
                        st.executeUpdate(actualizacion);
                        st.close();
                } catch (Exception ex) {
                        System.out.println(actualizacion);
                        System.out.println("Error al actualizar");
                        ex.printStackTrace();
                }
        }

        public ResultSet Consultar(String consulta, Connection conexion) {
                Statement st;
                ResultSet datos = null;
                try {
                        if (conexion == null) {
                                conexion = this.Conectar();
                        }
                        st = conexion.createStatement();
                        datos = st.executeQuery(consulta);
                } catch (Exception ex) {
                        System.out.println("Error al consultar");
                        System.out.println(consulta);
                        ex.printStackTrace();
                }
                return datos;
        }

        //Metodo para ver si hay datos (filas) en una tabla
        public boolean TablaSinDatos(String tabla) {
                String sql = "SELECT count(*) FROM \"" + tabla + "\"";
                Connection xcon = this.Conectar();
                ResultSet rs = this.Consultar(sql, xcon);
                try {
                        rs.next();
                        int cant = Integer.parseInt(rs.getString(1).toString());
                        xcon.close();
                        if (cant <= 0) {
                                return true;
                        } else {
                                return false;
                        }
                } catch (Exception ex) {
                        System.out.println("Error al verificar si hay datos en la bd");
                        ex.printStackTrace();
                }
                return false;
        }

        //Metodo que generará el siguiente código de una tabla
        public String generarCodigo(String tabla, String campo) {
                String rpta = "";
                String sql = "SELECT count(*) FROM " + tabla;
                Connection xcon = this.Conectar();
                try {
                        Statement st = xcon.createStatement();
                        ResultSet rs = st.executeQuery(sql);
                        rs.next();
                        int cant = Integer.parseInt(rs.getString(1).toString());
                        if (cant <= 0) {
                                rpta = "1";
                        } else {
                                sql = "SELECT max(" + campo + ") FROM " + tabla;
                                rs = st.executeQuery(sql);
                                rs.next();
                                cant = Integer.parseInt(rs.getString(1).toString()) + 1;
                                rpta = "" + cant;
                        }
                        xcon.close();
                        return rpta;
                } catch (SQLException ex) {
                        System.out.println("Error al generar codigo");
                        ex.printStackTrace();
                }
                return rpta;
        }

}
